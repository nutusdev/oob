<?php
/**
########################################
#OOB/N1 Framework [©2004,2006]
#
#  @copyright Pablo Micolini
#  @license BSD
######################################## 
*/
 
if (!seguridad :: isAllowed(seguridad_action :: nameConstructor('phpinfo','info','about')) )
{
	throw new OOB_exception("Acceso Denegado", "403", "Acceso Denegado. Consulte con su Administrador!", true);
} 

global $ari;

phpinfo();

$ari->t->display($ari->module->admintpldir(). "/phpinfo.tpl");
 
?>
