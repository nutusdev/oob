<?php
#OOB/N1 Framework [2008 - Nutus] - PM 

// Código por JPCOSEANI
// Script que genera el MAIN FRAME

global $ari;
$ari->popup = 1; // no mostrar el main_frame 

PhpExt_Javascript::sendContentType();



//--------------------------------------------------MENU TOP-----------------------------------------------------------------------------------------------------------//

$panel_left = new PhpExt_Panel();
$panel_left->setWidth(220)
			->setCollapsible(true)
		   ->setLayout( new PhpExt_Layout_BorderLayout() );
   
   
		   //->setBaseCssClass('x-plain')
   
$panel_icon = new PhpExt_Panel();
$panel_icon->setHeader(true);
$panel_icon->setHeight(50);

$src_imagen = '/images/logo.jpg';

$img = new PhpExt_Form_ImageField();
$img->setValue( $src_imagen );
$img->setId('imagen_logo_principal');
$img->setCssStyle('width:200px;height:33px;');		  

$panel_icon->addItem($img);
$panel_icon->setBodyStyle("padding:2px;background:#c3daf9;border:0px;padding-left:10px;");

$panel_left->addItem($panel_icon, PhpExt_Layout_BorderLayoutData::createNorthRegion());	



//trae los items del menu
$tree_loader = new  PhpExt_Tree_TreeLoader();
$tree_loader->setDataUrl("/admin/menu");

//armo el nodo root (no es visible)
$root = new PhpExt_Tree_AsyncTreeNode();
$root->setText("Principal Node")
     ->setDraggable(false)	 
     ->setId("Principal_Node")
	 ->setExpanded(true)
	 ->setCssClass("feeds-node")
     ->expandChildNodes(false);
	 
$menu_acordion = new PhpExt_Tree_TreePanel();
$menu_acordion->setTitle("Menu Principal")
	  		  ->setId("treePanel")		 			  
			  ->setLines(false)
			  ->setWidth(220)
			  ->setAutoScroll(true)
			  ->setCollapseFirst(false)
			  ->setSingleExpand(true)
			  ->setUseArrows(true)
			  ->setRootVisible(false)			 
			  ->setRoot($root)					 			
			  ->setLoader($tree_loader);
			  
$panel_left->addItem($menu_acordion, PhpExt_Layout_BorderLayoutData::createCenterRegion());			  
			
/// GRILLLA PENDIENTES
  
$store_pendientes = new PhpExt_Data_JsonStore();
$store_pendientes->setUrl("/auditoria/noticias/get_menu_lateral")
	 
	  ->setRoot("topics")
      ->setTotalProperty("totalCount")      
	  ->setAutoLoad(true);
	  
$store_pendientes->addField(new PhpExt_Data_FieldConfigObject("id"));
$store_pendientes->addField(new PhpExt_Data_FieldConfigObject("info_operador"));




// $redered_color = " function( v, params, record, rowIndex, colIndex, store ){ ";
			
					// $redered_color.= 'params.attr = \'style="color:#ADDDDDD;"\'';
	
// $redered_color.= "return v;} ";
$redered_color = " function( v, params, record, rowIndex, colIndex, store ){ 
params.attr = 'style=\"color:#DF3A01;\"';
// params.attr = 'style=\"background:#DF3A01;\"';
	return v;
} ";
			   
//SE AGREGAN LAS COLUMNAS A LA GRILLA   
$col_model_pendientes = new PhpExt_Grid_ColumnModel();

// $col_model_pendientes->addColumn(PhpExt_Grid_ColumnConfigObject::createColumn("","info_operador",null,190));
 // $col_model_pendientes ->addColumn(PhpExt_Grid_ColumnConfigObject::createColumn("","info_operador",null,190,new PhpExt_JavascriptStm($redered_color)));
$col_model_pendientes->addColumn(PhpExt_Grid_ColumnConfigObject::createColumn("","info_operador",null,205,null, new PhpExt_JavascriptStm($redered_color)));


$grid_pendientes = new PhpExt_Grid_GridPanel();
$grid_pendientes->setHeight(95)	     
				 
				 ->setSelectionModel(new PhpExt_Grid_RowSelectionModel())
				 ->setColumnModel( $col_model_pendientes )	 
				 ->setStore( $store_pendientes )	 
				 // ->setVisibleHeader(false)
				 ->setEnableHeaderMenu(false)
				 ->setEnableColumnMove(false)
				 ->setBorder(false)	 
				 ->setFrame(false)
				 // ->setWidth(0.90)
				 ->setLoadMask(true);	

				 
$grid_pendientes_render = "

var store = grid.getStore();
	
	 /////////////////////CODIGO PARA EL REFRESCO DE LA GRILLA///////////////////////////////
var task = {
   run: function() {
        store.load()
   },
  
   interval: 60000 //60 segundines
}

var runner = new Ext.util.TaskRunner();
 
runner.start(task);

";

$grid_pendientes->setEnableKeyEvents(true);
$grid_pendientes->attachListener( "render", new PhpExt_Listener( PhpExt_Javascript::functionDef( null, $grid_pendientes_render , array( "grid" ) )) );			   		   	 			 
				 

// $panel_left->addItem($grid_ultimas_noticias, PhpExt_Layout_BorderLayoutData::createSouthRegion());		

	
$panel_buttons = new PhpExt_Panel();
$panel_buttons->setId('panel_buttons');
$panel_buttons->setHeight(150);
$panel_buttons->setBaseCssClass('x-plain');
// $panel_buttons->addItem($grid_pendientes); /// SACADO HASTA NUEVO AVISO!!!

$panel_left->addItem($panel_buttons, PhpExt_Layout_BorderLayoutData::createSouthRegion());


//----------------------------------------PANTALLA PRINCIPAL-------------------------------------------------------------------------------------------------------

$tab_layout = new PhpExt_Layout_TabLayout();
$tab_layout->setDeferredRender(false);

$principal = new PhpExt_TabPanel();
$principal->setActiveTab(0)
		  ->setId("TabPanel")
		  ->setResizeTabs(false)
		  ->setHideMode(PhpExt_Component::HIDE_MODE_OFFSETS)
		  ->setLayout($tab_layout)
		  ->setEnableKeyEvents(true)
		  ->setenableTabScroll(true);
		  
		  
$onrender = "




//beforeclose
t.on( 'beforeremove', function(t,c){
	
	if (c.id == 'panel_bienvenido')
	{
		return false;
	} 

	var cnx = new Ext.data.Connection();
	Ext.Ajax.request({ url : '/admin/closetab',
					method : 'POST',
					params : 'tab_id=' + c.tabid 					   
					 });

});
";		  

$principal->attachListener("render", new PhpExt_Listener( PhpExt_Javascript::functionDef(null, $onrender, array("t","r","i") )));		  

$module = new OOB_module('About');

$template_dir = $module->admintpldir(). "/about.tpl";
$html = $ari->t->fetch( $template_dir ); 

$bienvenido = new PhpExt_Panel();
$bienvenido->setTitle("Bienvenido")
		   ->setId("panel_bienvenido")			
		   ->setAutoScroll(true)
		   ->setBodyStyle("padding:10px 10px 0")
		   ->setHtml($html);
		   
$principal->addItem($bienvenido, new PhpExt_Layout_TabLayoutData(true));



$add_tab_function = "


Ext.grid.GridPanel.prototype.getSelections = function()
{
	return this.getSelectionModel().getSelections();
}

var panel_buttons = Ext.getCmp('panel_buttons');

 
	 var menu = new Ext.menu.Menu({
			  cls  : 'ux-start-menu',
		autoHeight : true,
			 xtype : 'menu',        
          defaults : { xtype:'menuitem' },
          floating : false,
             split : true,
			 items : [{
                text : 'Bienvenido " .  $ari->get("user")->name() . "',
			 iconCls :'user',
             handler : function(){
					addTab('Mi Cuenta','/seguridad/user/update',true,'',false);			   
             }
            },{
                text : 'Salir',
			 iconCls : 'close',
			 handler : function(){
						Ext.Msg.show({ title: 'Nutus',
					   msg: '¿Est&aacute; seguro que desea salir?',
					   width: 300,
					   buttons: { cancel:'Cancelar', yes:'Salir', ok:'Salir y Guardar' },
					   multiline: false,
					   fn: function( Result ){
					   
						if(Result == 'yes'){
							window.location = '/seguridad/logout';
						}
						
						if(Result == 'ok'){
							window.location = '/seguridad/logoutsave';
						}
						
						}}); 
			 }
			 }
			 ]
        });

 
panel_buttons.add(menu);
panel_buttons.doLayout();

//oob_download
Ext.DomHelper.append(document.body, {
                    tag: 'form',
                    id:'download_form',
                    frameBorder: 0,
                    width: 0,
                    height: 0,
                    css: 'display:none;visibility:hidden;height:0px;'
					}); 

function oob_download( url, params ){

var form = document.getElementById('download_form');

var inputs = form.getElementsByTagName('input');

for (i = 0; i < inputs.length; i++){
   form.removeChild(inputs[i]); 
}

if( Ext.isArray(params) ){
	Ext.each( params, function( item, index ){
		el = document.createElement('input');
		el = form.appendChild(el);
		el.name = item.name;
		el.type = 'hidden';
		el.value = item.value;	
	});
}

form.method = 'post';
form.action = url;
form.submit();

}
Ext.apply( Ext,{ oob_download : oob_download } );
//fin oob_download					

var msgconfig = {
		   title :'Error',
		progress : false,
		    wait : false,
		     msg : 'Se produjo un error al cargar la pagina.',
		 buttons : Ext.Msg.OK,		 
		    icon : Ext.MessageBox.ERROR ,
			  fn : function(c,t,o){
				Ext.MessageBox.getDialog().setTitle(''); 
				Ext.MessageBox.getDialog().hide();
			  }	
		}



Ext.Ajax.on('requestexception',function(request,response,f,g,h){
			
	switch( response.status ){
	case 401:

	var loginDialog = new Ext.ux.form.LoginDialog({
				modal : true,
				title : 'Nutus',
			  message : 'Por su seguridad debe logearse nuevamente,<br /> ya que no ha utilizado el sistema por más de 30 minutos',
		usernameLabel : 'Usuario',
		passwordLabel : 'Contrase&ntilde;a',
		 cancelButton : 'Cerrar',
		  loginButton : 'Enviar',
		  failMessage : 'Usuario o contrase&ntilde;a no v&aacute;lida.',
				  url : '/seguridad/login_ajax'			
			});

	
	loginDialog.show();

	loginDialog.on('success',function(){
	request.request(f);
	});

	break;	
	case 403:
	
			var msgconfig = {
		   title :'Permiso denegado',				    
		     msg : 'Usted no tiene los permisos necesarios para realizar la acci&oacute;n',
		 buttons : Ext.Msg.OK,		 
		    icon : Ext.MessageBox.ERROR ,
			  fn : function(c,t,o){
				Ext.MessageBox.getDialog().setTitle(''); 
				Ext.MessageBox.getDialog().hide();
			  }	
		}
				
		Ext.Msg.show(msgconfig);
		Ext.getCmp('status_bar').clearStatus({useDefaults:true});
	
	break;
	case 400:
		
		var msgconfig = {
		   title :'Error 400',				    
		     msg : response.getResponseHeader('message'),
		 buttons : Ext.Msg.OK,		 
		    icon : Ext.MessageBox.ERROR ,
			  fn : function(c,t,o){
				Ext.MessageBox.getDialog().setTitle(''); 
				Ext.MessageBox.getDialog().hide();
			  }	
		}
	
		Ext.MessageBox.hide();	 
		var win = Ext.MessageBox.getDialog();		
		
		win.on('beforehide',function(){
			
			if( this.title == 'Error 400'){
					return false;
			}		
		});
		
		Ext.Msg.show(msgconfig);
		Ext.getCmp('status_bar').clearStatus({useDefaults:true});
		
	break;
	case 404:
		var msgconfig = {
		   title :'Error 404',				    
		     msg : response.getResponseHeader('message'),
		 buttons : Ext.Msg.OK,		 
		    icon : Ext.MessageBox.ERROR ,
			  fn : function(c,t,o){
				Ext.MessageBox.getDialog().setTitle(''); 
				Ext.MessageBox.getDialog().hide();
			  }	
		}
		Ext.Msg.show(msgconfig);
		Ext.getCmp('status_bar').clearStatus({useDefaults:true});	
	break;
	case 500:		
        Ext.MessageBox.hide();	 
		var win = Ext.MessageBox.getDialog();		
		
		win.on('beforehide',function(){
			
			if( this.title == 'Error'){
					return false;
			}		
		});
		
		Ext.Msg.show(msgconfig);
		Ext.getCmp('status_bar').clearStatus({useDefaults:true});
		 
	break;
	case 9001:
		Ext.MessageBox.alert('Nutus',response.getResponseHeader('message') ); 
		Ext.getCmp('status_bar').clearStatus({useDefaults:true});
	break;	
	}
});



Ext.Ajax.on('requestcomplete', function(request,response,f,g,h){
try
  { 
  
    //Ext.MessageBox.updateProgress(1);
    //Ext.MessageBox.hide();	
	//alert(response.getResponseHeader('Content-Type'));
	

	 
		
  }
catch(err)
  {
  //alert(err.description);
  }
  


}, this);



Ext.Ajax.request({url: '/admin/getcache',
		  method: 'POST',
  		 success: function(responseObject){												
							   json = Ext.decode(responseObject.responseText);							   
							   Ext.each( json, function(i){									
									addTab(i.title,i.url,true,i.params,true,i.id);
							   });						   
					   }				   
				});



var panel_tabs = Ext.getCmp('TabPanel');
panel_tabs.on('tabchange',function(tabPanel, tab){                
                    Ext.History.add(tab.id);              
});
				
Ext.History.on('change', function(token){
		alert(token);
		if(token){
            var parts = token.split(tokenDelimiter);
            var tabPanel = Ext.getCmp(parts[0]);
            var tabId = parts[1];
            
            tabPanel.show();
            tabPanel.setActiveTab(tabId);
        }else{           
            tp.setActiveTab(0);
            tp.getItem(0).setActiveTab(0);
        }


});

 
//funcion para agregar las tabs 
function addTab( Title, Url, Add, Params, cache, tab_id ){

var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
var cnx = new Ext.data.Connection(); //creo un nuevo objeto conexion
var tab_id; //defino la variable tab_id, tiene el id de la tab que se va agregar
			
	//pongo la barra de estado(cargando...)
	Ext.getCmp('status_bar').showBusy();
			
	//obtengo un id unico para el contenedor de los contenidos que voy a cargar
	var id = Ext.id();

	//function para agregar definitivamente la tab
	var add = function( tab_id ){
	
	
			//si quiero agregar una tab nueva
									

		Ext.Ajax.request( { url : Url,
						method : 'POST',						
						params : Params,
						failure : function(){
							Ext.Ajax.request({ url : '/admin/closetab',
														method : 'POST',
														params : 'tab_id=' + tab_id + '&selected=' + panel_tabs.getActiveTab().tabid 					   
														 });
						},
					   success : function( responseObject ){												
						var respuesta = responseObject.responseText;	

									if(Add){
									
										   var tab = new Ext.Panel({
													 tabid : tab_id,
													 title : Title,
													layout : 'fit',													   
												  closable : true,
											deferredRender : false,
													  html : '<div style=\"height:100%;width:100%;\" id=\"' + id + '\"></div>',
												autoScroll : true							 							
										   });
										   
										   panel_tabs.add(tab);
										   Ext.History.add(tab.id);
										   tab.show();													   					
									}
									else //si quiero agregar en la tab que esta activa
									{
									
										
										var active_tab = panel_tabs.getActiveTab();
										
										var cnx = new Ext.data.Connection();
										Ext.Ajax.request({ url : '/admin/closetab',
														method : 'POST',
														params : 'tab_id=' + active_tab.tabid 					   
														 });
										active_tab.setTitle(Title);
										active_tab.tabid = tab_id;			
										Ext.History.add(active_tab.id);
										active_tab.body.dom.innerHTML = '<div style=\"height:100%;width:100%;\" id=\"' + id + '\"></div>';
									}
																		
									//una vez insertado el contenedor con el id unico , se procede a insertar los
									//datos en el mismo
									
									//si el contenido a cargar es un html
									if( responseObject.getResponseHeader('Content-Type') == 'text/html' ){		
										Ext.get(id).dom.innerHTML = respuesta;			  
									}
									else //si el contenido es un json(extjs)
									{
										//ejecuto la respuesta y hago un render de la variable contenido
										//sobre el contenedor
										var active_tab = panel_tabs.getActiveTab();
										eval(respuesta);				
										contenido.render(Ext.get(id));									
										
										//llamo el evento para que aplique los filtros
																				
											contenido.fireEvent( 'applyfilters', tab_id );
											active_tab.grid = contenido;
											
											//agregamos el evento keydown en caso que sea un grid.
											if( active_tab.grid.getXType() == 'grid' 
												|| active_tab.grid.getXType() == 'editorgrid'
											){								
												active_tab.grid.on( 'keydown',
													function (e){
														if(e.browserEvent.keyCode == e.RETURN){															
															   
															if( active_tab.grid.getTopToolbar() != undefined ){									
																if( active_tab.grid.getTopToolbar().getXType() == 'toolbar' ){
																	var button_ver = active_tab.grid.getTopToolbar().items.find( function(c){ return ( c.text == 'Ver detalles' ) } );
																	if( button_ver != null ){
																		if(button_ver.getXType() == 'tbbutton'){
																			button_ver.fireEvent('click');
																		}	
																	}		
																}
															}															   
														}
													}, this ); 												
											}//end if
											//------>end event keydown.
											
											//agrego el evento para los botones cerrar de los formularios.
											if( active_tab.grid.getXType() == 'form' ){
												var buttons = active_tab.grid.buttons;
												if( buttons )
												{
													for(i = 0 ; i < buttons.length; i++)
													{
														if( buttons[i].text == 'Cerrar'	){
															buttons[i].on( 'click', function(){
																
																	Ext.Msg.show({ title: 'Nutus',
																	   msg: '¿Est&aacute; seguro que desea cerrar el formulario?',
																	   width: 300,
																	   buttons: { cancel:'Cancelar', yes:'Si' },
																	   multiline: false,
																	   fn: function( Result ){																	
																		if(Result == 'yes'){
																			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
																			panel_tabs.remove(active_tab);
																		}																
																	}});															
																
															},this );
															break;
														}
													}//end for
												}//end if
											}//end if	
											
									}//end if		
									
														
									Ext.getCmp('status_bar').clearStatus({useDefaults:true});
									
							return true;			
					}});


			}//end function addtab

		
		//si !cache , quiere decir que la tab no esta en cache, por lo tanto llamo a newtab
		//para que la cachee y me devuelve el tab_id
		if(!cache){
		
			  var data = {    url : Url,
						    title : Title,
						   params : Params		  
						 }
			
			  Ext.Ajax.request({url: '/admin/newtab',
						method: 'POST',
						params: { data : Ext.encode(data) } ,
					   success: function(responseObject){
							   json = Ext.decode(responseObject.responseText);							  							   
							   //este tab_id luego es usado por la funcion tab_id
							   tab_id = json.id;							   							   
							   add(tab_id); 							   							   					   								
					   }
				});		
		}
		else
		{
					add(tab_id); 							   
		}		
	
		
	}
	
				
	
	Ext.apply( Ext,{ addTab : addTab } );
	
	
	//shortcut close active tab	
	var map = new Ext.KeyMap( document, [
	{
		key: \"c\",
		ctrl:true,
		 alt:true,
		fn: function(){
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
				 if(active_tab){
					panel_tabs.remove(active_tab);
				 }//end if										
		}
	}
	]);//end shortcut
	
	//shortcut para recargar grid.	
	var map = new Ext.KeyMap( document, [
	{
		key: \"r\",
		ctrl:true,
		 alt:true,
		fn: function(){
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
				 if(active_tab)
				 {										
						if(active_tab.grid){							
							if( active_tab.grid.getXType() == 'grid' 
								|| active_tab.grid.getXType() == 'editorgrid'
							){								
								if( active_tab.grid.getBottomToolbar() != undefined ){
									if( active_tab.grid.getBottomToolbar().getXType() == 'paging' ){
										active_tab.grid.store.reload();
									}
								}									
							}
						}					
				 }//end if										
		}
	}
	]);//end shortcut
	
	//shortcut para cuando hay un boton 'nuevo' en el toolbar de un grid.	
	var map = new Ext.KeyMap( document, [
	{
		key: \"n\",
		ctrl:true,
		 alt:true,
		fn: function(){
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
				 if(active_tab)
				 {										
						if(active_tab.grid){							
							if( active_tab.grid.getXType() == 'grid' 
								|| active_tab.grid.getXType() == 'editorgrid'
							){								
								if( active_tab.grid.getTopToolbar() != undefined ){									
									if( active_tab.grid.getTopToolbar().getXType() == 'toolbar' ){
										var button_nuevo = active_tab.grid.getTopToolbar().items.find( function(c){ return ( c.text == 'Nuevo' || c.text == 'Nueva' ) } );
										if( button_nuevo != null ){
											if(button_nuevo.getXType() == 'tbbutton'){
												button_nuevo.fireEvent('click');
											}	
										}		
									}
								}									
							}
						}					
				 }//end if										
		}
	}
	]);//end shortcut
	
	//short cut para la tecla 'flecha izquierda'
	var map = new Ext.KeyMap( document, [
	{
		key:37,
		ctrl:true,
		 alt:true,
   stopEvent:true,
		fn: function(){
			
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
				 if(active_tab)
				 {										
						if(active_tab.grid){							
							if( active_tab.grid.getXType() == 'grid' 
								|| active_tab.grid.getXType() == 'editorgrid'
							){								
								if( active_tab.grid.getBottomToolbar() != undefined ){
									if( active_tab.grid.getBottomToolbar().getXType() == 'paging' ){
										var button_prev = active_tab.grid.getBottomToolbar().items.find( function(c){ return ( c.iconCls == 'x-tbar-page-prev' ) } );
										if( !button_prev.disabled ){
											active_tab.grid.getBottomToolbar().movePrevious();
										}	
									}
								}									
							}
						}					
				 }//end if		
		}
	}]);	
	
	//short cut para la tecla 'flecha derecha'
	var map = new Ext.KeyMap( document, [
	{
		key:39,
		ctrl:true,
		 alt:true,
   stopEvent:true,
		fn: function(){
			
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
				 if(active_tab)
				 {										
						if(active_tab.grid){							
							if( active_tab.grid.getXType() == 'grid' 
								|| active_tab.grid.getXType() == 'editorgrid'
							){								
								if( active_tab.grid.getBottomToolbar() != undefined ){
									if( active_tab.grid.getBottomToolbar().getXType() == 'paging' ){
										var button_next = active_tab.grid.getBottomToolbar().items.find( function(c){ return ( c.iconCls == 'x-tbar-page-next' ) } );
										if( !button_next.disabled ){
											active_tab.grid.getBottomToolbar().moveNext();
										}	
									}
								}									
							}
						}					
				 }//end if		
		}
	}]);
	
	//short cut para mover a la tab anterior
	var map = new Ext.KeyMap( document, [
	{
		key:40,
		ctrl:true,
		 alt:true,
   stopEvent:true,
		fn: function(){
			
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
			var items = panel_tabs.items.items;
				 if(active_tab)
				 {					
					var total_tabs = items.length;
					for(i = 0 ; i < items.length; i++)
					{
						if (active_tab.id == items[i].id) {
							var next = (i - 1);
							if (next < 0)
							{							
								next = (total_tabs - 1);
							}							
							panel_tabs.setActiveTab(items[next].id)
						}
					}//end for
				 }//end if		
		}
	}]);
	
	//short cut para mover a la tab proxima
	var map = new Ext.KeyMap( document, [
	{
		key:38,
		ctrl:true,
		 alt:true,
   stopEvent:true,
		fn: function(){
			
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
			var items = panel_tabs.items.items;
				 if(active_tab)
				 {					
					var total_tabs = items.length;
					for(i = 0 ; i < items.length; i++)
					{
						if (active_tab.id == items[i].id) {
							var next = (i + 1);
							if (next >= total_tabs)
							{							
								next = 0;
							}							
							panel_tabs.setActiveTab(items[next].id)
						}
					}//end for
				 }//end if		
		}
	}]);
	
	
				
	//stop backspace key
	// var map = new Ext.KeyMap(document, [
	// {	key: Ext.EventObject.BACKSPACE,
		// stopEvent: true
	// }]);//end shortcut	

	//esto dejarlo siempre al ultimo por que hace el fadeout del precargador( osea lo oculta )
	var hideMask = function () {
        Ext.get('loading').remove();
        Ext.fly('loading-mask').fadeOut({
            remove:true
        });
    }
	
	//shortcut para detectar el enter en el grid.
	var map = new Ext.KeyMap( document, [
	{
		key: \"s\",
		ctrl:true,
		 alt:true,
   stopEvent:true,
		fn: function(){
			var panel_tabs = Ext.getCmp('TabPanel'); //obtengo el tabpanel que contiene todas las tabs
			var active_tab = panel_tabs.getActiveTab();
				 if(active_tab)
				 {										
						if(active_tab.grid){							
							if( active_tab.grid.getXType() == 'grid' 
								|| active_tab.grid.getXType() == 'editorgrid'
							){								
								
								if( active_tab.grid.getTopToolbar() != undefined ){									
									if( active_tab.grid.getTopToolbar().getXType() == 'toolbar' ){
										var button_save = active_tab.grid.getTopToolbar().items.find( function(c){ return ( c.text == 'Guardar cambios' ) } );
										if( button_save != null ){
											if(button_save.getXType() == 'tbbutton'){
												button_save.fireEvent('click');
											}	
										}		
									}
								}
								
							}//end if
							
							if( active_tab.grid.getXType() == 'form' ){
								var buttons = active_tab.grid.buttons;
								for(i = 0 ; i < buttons.length; i++)
								{
									if( buttons[i].text == 'Grabar' ||
										buttons[i].text == 'Aceptar'
									){										
										buttons[i].handler();									
										break;
									}
								}
								
							}
						}//end if					
				 }//end if										
		}
	}
	]);//end shortcut
	
		
	//confirm on window close
	// Ext.EventManager.on(window, 'beforeunload', function(e){
	
	// e.browserEvent.preventDefault();
	
	// Ext.Msg.show({ title: 'Nutus',
				   // msg: '¿Est&aacute; seguro que desea salir?',
				   // width: 300,
				   // buttons: { cancel:'Cerrar', yes:'Salir', ok:'Salir y Guardar' },
				   // multiline: false,
				   // fn: function( Result ){
					
					// if(Result == 'yes'){
						// window.location = '/seguridad/logout';
					// }					
					// if(Result == 'ok'){
						// window.location = '/seguridad/logoutsave';
					// }			  		
					// if(Result == 'cancel'){
						// e.stopEvent();
					// }	
		// }});
	// }, this); 
	
			
    hideMask.defer(250);			
	
	
	";
									


$add_tab_invoke = PhpExt_Javascript::callfunction("addTab",array("n.id[0]","n.id[1]","true","''","false"))->output()."return false;";									
$add_tab_invoke_html = PhpExt_Javascript::callfunction("addTab",array("n.id[0]","n.id[1]","false","''","false"))->output()."return false;";									

$get_tree_panel = PhpExt_Element::getCmp('treePanel');
$if_leaf=PhpExt_Javascript::functionNoDef("if",$add_tab_invoke,array("n.leaf"));

$if_leaf_html=PhpExt_Javascript::functionNoDef("if",$add_tab_invoke_html,array("n.leaf"));

$add_tab_onclick= PhpExt_Javascript::functionDef(null,$if_leaf,array("n"));   
$add_tab_oncontextmenu= PhpExt_Javascript::functionDef(null,$if_leaf_html,array("n"));   

$output_add_tab_onclick=$get_tree_panel->on("click", $add_tab_onclick);
$output_add_tab_oncontextmenu=$get_tree_panel->on("contextmenu", $add_tab_oncontextmenu);
												
$output_add_tab_function=new PhpExt_JavascriptStm($add_tab_function);


//-----------------------------------------------------------BARRA DE ESTADO--------------------------------------------------------------------------------------------

$function_win_open=PhpExt_Javascript::callfunction("window.open",array("'http://www.nutus.com.ar'"));
$handler_abrir_pagina=PhpExt_Javascript::functionDef("",$function_win_open,array("e"));

$function_win_open2=PhpExt_Javascript::callfunction("window.open",array("'http://soporte.nutus.info'"));
$handler_abrir_pagina2=PhpExt_Javascript::functionDef("",$function_win_open2,array("e"));

$barra_estado=new PhpExt_Toolbar_StatusBar();
$barra_estado->setId("status_bar");
$barra_estado->setDefaultText("Terminado");
$barra_estado->addButton("ayuda","Ayuda on-line",null,$handler_abrir_pagina2);
$barra_estado->addButton("Pagina","&#169; Nutus ".date('Y'),null,$handler_abrir_pagina);



$status_bar = new PhpExt_Panel();
$status_bar->setBottomToolbar($barra_estado);



//USO UN VIEWPORT YA QUE SE ADAPTA AL ANCHO DE LA PAGINA
//creo uno nuevo y le agrego todos los items setando el area
		  
		  $contenedor_menu = new PhpExt_Viewport();		  
		  $contenedor_menu->setLayout(new PhpExt_Layout_BorderLayout());
		  $contenedor_menu->addItem($panel_left, PhpExt_Layout_BorderLayoutData::createCenterRegion());
		  
//panel_left
$contenedor= new PhpExt_Viewport();
$contenedor->setLayout(new PhpExt_Layout_BorderLayout());

$contenedor->addItem($panel_left,PhpExt_Layout_BorderLayoutData::createWestRegion());
$contenedor->addItem($principal, PhpExt_Layout_BorderLayoutData::createCenterRegion());
$contenedor->addItem($status_bar, PhpExt_Layout_BorderLayoutData::createSouthRegion());

//FUNCTIONS DE USO GENERAL

$format_money = "	
function FormatMoney(v,sign){				
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + '.00' : ((v*10 == Math.floor(v*10)) ? v + '0' : v);
    v = String(v);        
        if(v.charAt(0) == '-'){
            return '-' + sign + v.substr(1).replace('.',',');
        }
    return sign +  v.replace('.',',');
}
";



$unformat_money = "
function unformatMoney(num) {
	var value = num.replace(',','.');		
	return value.replace(/([^0-9\.\-])/g,'')*1;	
}
";

$constantes = "";
$constantes.= "Ext.Ajax.timeout = 600000;";
$constantes.= "var PAGE_SIZE=" . PAGE_SIZE . ';';

$var_dump = "
function var_dump(obj) 
	{
		var out = '';
		for (var i in obj) {
			out += i + \": \" + obj[i] + \"\\n\";
		}

		alert(out);

	}
";


echo PhpExt_Ext::OnReady(
	 PhpExt_QuickTips::init(),
	 $contenedor->getJavascript(false, "Contenedor"),	
	 $contenedor->render(PhpExt_Javascript::inlineStm("document.body")),		
	 $constantes,
	 $var_dump,
	 $output_add_tab_function->output(),	 
	 $output_add_tab_onclick,
	 $output_add_tab_oncontextmenu,
	 $unformat_money,
	 $format_money	 
	 );
?>