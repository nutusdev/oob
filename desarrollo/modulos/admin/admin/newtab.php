<?php
global $ari;
$ari->popup = 1;

if( !isset($_POST['data'])){
	throw new OOB_Exception_400("La variable [data] no esta definida");
}

$data = json_decode( $_POST['data'] ,true);
if(!isset($data['params']))
{
	$data['params'] = '';
}

if( $tab_cache = new admin_session_state() )
{
	if( $tab_id = $tab_cache->add_tab_cache( $data['url'] , $data['title'] , $data['params'] ) ){	
			
		//RESULTADO
		$obj_comunication = new OOB_ext_comunication();
		$obj_comunication->set_data( array( 'id' => $tab_id ) );
		$obj_comunication->send(true,true);
		
	}
	else
	{
		echo false;
	}
}	
?>
