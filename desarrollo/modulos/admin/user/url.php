﻿<?
/**
########################################
#OOB/N1 Framework [©2011]
#
#  @copyright Pablo Micolini
#  @license BSD
######################################## 
*/


global $ari;
$ari->popup = true;

echo "Invalid attemp to access administrative interface registered.<br>";
echo "Remember to install your client SSL certificate to acccess the system, or contact tech support.";
$ari->error->addError('ADMIN-FAIL','Intento de acceder al adminstrador',true);


?>