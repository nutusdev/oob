Ext.form.ImageField = Ext.extend(Ext.form.Field, {
    autoCreate: {tag: 'img'},
	value : ''
    ,setValue: function(new_value){
        if (new_value == undefined || new_value == null) {			
            this.el.dom.src = '/images/no_image.png';
        } else {
            this.el.dom.src = new_value;
        }
    }
    ,initValue : function(){
          this.setValue(this.value);
    }

    ,initComponent: function() {
        Ext.apply(this, function(){
			
        });

        Ext.form.ImageField.superclass.initComponent.apply(this);
    }
});

Ext.reg('imagefield', Ext.form.ImageField);

