
Ext.ux.MonthPickerPlugin = function() {
    var picker;
    
    this.init = function(pk) {
        //Date.defaults.d = 1;
        picker = pk;
        picker.onTriggerClick = picker.onTriggerClick.createSequence(onClick);
        picker.getValue = dateFieldGetValue;
        picker.beforeBlur = beforeFieldBlur;
    };
    
    function dateFieldGetValue() {
        var d = Date.defaults.d;
        Date.defaults.d = 1;
        var ret = this.parseDate(Ext.form.DateField.superclass.getValue.call(this)) || "";
        Date.defaults.d = d;
        return ret;
    }
    
    function onClick(e, el, opt) {
        var p = picker.menu.picker;
        p.activeDate = p.activeDate.getFirstDateOfMonth();
        if (p.value) {
            p.value = p.value.getFirstDateOfMonth();
        }
        
        // DatePicker.showMonthPicker
        if(!p.disabled){
            p.createMonthPicker();
            var size = p.el.getSize();
            p.monthPicker.setSize(size);
            p.monthPicker.child('table').setSize(size);
            p.mpSelMonth = (p.activeDate || p.value).getMonth();
            p.updateMPMonth(p.mpSelMonth);
            p.mpSelYear = (p.activeDate || p.value).getFullYear();
            p.updateMPYear(p.mpSelYear);
            // replace the slide in with a show
            p.monthPicker.show();
            
            p.mun(p.monthPicker, 'click', p.onMonthClick, p);
            p.mun(p.monthPicker, 'dblclick', p.onMonthDblClick, p);
            p.onMonthClick = p.onMonthClick.createSequence(pickerClick);
            p.onMonthDblClick = p.onMonthDblClick.createSequence(pickerDblclick);
            p.mon(p.monthPicker, 'click', p.onMonthClick, p);
            p.mon(p.monthPicker, 'dblclick', p.onMonthDblClick, p);
        }
    }
    
    function pickerClick(e, t) {
        var p = picker.menu.picker;
        var el = new Ext.Element(t);
        if (el.is('button.x-date-mp-cancel')) {
            picker.menu.hide();
        } else if(el.is('button.x-date-mp-ok')) {
            p.setValue(p.activeDate);
            p.fireEvent('select', p, p.value);
        }
    }
    
    function pickerDblclick(e, t) {
        var p = picker.menu.picker;
        p.setValue(p.activeDate);
        p.fireEvent('select', p, p.value);
    }
    
    function beforeFieldBlur() {
        var d = Date.defaults.d;
        Date.defaults.d = 1;
        var v = this.parseDate(this.getRawValue());
        Date.defaults.d = d;
        if (v) {
            this.setValue(v);
        }
    }
};

Ext.preg('monthPickerPlugin', Ext.ux.MonthPickerPlugin); 