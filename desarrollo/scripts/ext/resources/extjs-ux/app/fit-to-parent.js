/**
 * Ext.ux.plugins.FitToParent plugin for Ext.gridpanel
 *
 * @author  Juan Pablo Coseani
 * @date    24/11/2009
 *
 * @class Ext.ux.plugins.FitToParent
 * @extends Ext.util.Observable
 */

Ext.namespace('Ext.ux.plugins');
Ext.ux.plugins.FitToParent = function(config) {
	    Ext.apply(this, config);
}

Ext.extend( Ext.ux.plugins.FitToParent, Ext.util.Observable, 
{
    init : function(element) 
	{
		element.on('render',function(c)
		{
		
			var tabpanel = Ext.get('TabPanel');
			element.monitorResize = true;
			// tabpanel.doLayout();
			
			var size = tabpanel.getViewSize();
			// var pos = tabpanel.getPosition(true);
			

			element.doLayout = element.doLayout.createInterceptor(function()
			{
				element.setSize((size.width-2),(size.height-29));		
					// element.setSize((size.width-pos[0]),(size.height-pos[1]));		
				// element.setSize(size.width,size.height);	
				
				
			  });  
			  
		});
	}
});
