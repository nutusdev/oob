<?php

include_once 'PhpExt/Layout/ContainerLayout.php';

class PhpExt_Layout_VboxLayout extends PhpExt_Layout_ContainerLayout  {
    
   
    public function getLayoutKey() {
        return PhpExt_Layout_ContainerLayout::LAYOUT_VBOX;
    }
    
	public function __construct() {
	    parent::__construct();
	    $this->setExtClassInfo("Ext.layout.VBoxLayout", "vbox");	    	   

	    $validLayoutProps = array(
		"align"
	    );
	    $this->addValidConfigProperties($validLayoutProps);
	    
	    $this->addValidLayoutDataClassName("PhpExt_Layout_VboxLayoutData");
	}
	
	public function setAlign($value) {
        $this->setExtConfigProperty("align", $value);
        return $this;
    }
	
	public function getAlign() {
        return $this->getExtConfigProperty("align");
    }
	
	
}
?>