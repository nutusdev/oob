<?php

include_once 'PhpExt/Layout/ContainerLayoutData.php';

class PhpExt_Layout_VboxLayoutData extends PhpExt_Layout_ContainerLayoutData 
{

	const LEFT = 'left';
    const CENTER = 'center';
    const STRETCH = 'stretch';
    const STRETCHMAX = 'stretchmax';
    
	public function setAlign($value) {
        $this->setLayoutProperty("align", $value);
        return $this;
    }
	
	public function getAlign() {
        return $this->getLayoutProperty("align");
    }
	
public function __construct() {
	parent::__construct();		
		
}


}

?>