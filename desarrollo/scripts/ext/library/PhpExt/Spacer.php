<?php
include_once 'PhpExt/Component.php';

class PhpExt_Spacer extends PhpExt_Component  
{

	public function __construct() {
		parent::__construct();
		$this->setExtClassInfo("Ext.Spacer", "spacer");
		
		$validProps = array(		    
		);	
		$this->addValidConfigProperties($validProps);		
	}//end function

}//end class

?>