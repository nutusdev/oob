<?php
/**
 * PHP-Ext Library
 * http://php-ext.googlecode.com
 * @author Sergei Walter <sergeiw[at]gmail[dot]com>
 * @copyright 2008 Sergei Walter
 * @license http://www.gnu.org/licenses/lgpl.html
 * @link http://php-ext.googlecode.com
 * 
 * Reference for Ext JS: http://extjs.com
 * 
 */

/**
 * @see PhpExt_Form_Field
 */
include_once 'PhpExt/Form/TextField.php';

/**
 * Basic text field. Can be used as a direct replacement for traditional text inputs, or as the base class for more sophisticated input controls (like {@link PhpExt_Form_TextArea} and {@link PhpExt_Form_ComboBox}).
 * @package PhpExt
 * @subpackage Form
 */
class PhpExt_Form_FileUploadField extends PhpExt_Form_TextField 
{	

	public function setButtonText($value) {
		$this->setExtConfigProperty("buttonText", $value);
		return $this;
	}	
	/**
	 * A custom error message to display in place of the default message provided for the vtype currently set for this field (defaults to ''). Only applies if vtype is set, else ignored.
	 * @return boolean
	 */
	public function getButtonText() {
		return $this->getExtConfigProperty("buttonText");
	}
	public function __construct() {
		parent::__construct();
		$this->setExtClassInfo("Ext.form.FileUploadField","fileuploadfield");
		
		$validProps = array(
		    "allowBlank",
		    "blankText",					
		    "disableKeyFilter",
		    "emptyClass",
		    "emptyText",
		    "grow",
		    "growMax",
		    "growMin",
		    "maskRe",
		    "maxLength",
		    "maxLengthText",
		    "minLength",
		    "minLengthText",
		    "regex",
		    "regexText",
		    "selectOnFocus",
		    "validator",
		    "vtype",
		    "vtypeText",			
		    "buttonText"			
		);
		$this->addValidConfigProperties($validProps);
		

	}	 
	

	
}

