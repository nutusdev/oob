<?php

include_once 'PhpExt/Toolbar/Toolbar.php';

class PhpExt_ButtonGroup extends PhpExt_Toolbar_Toolbar  
{

	public function __construct() {
		parent::__construct();
		$this->setExtClassInfo("Ext.ButtonGroup", "buttongroup");
		
		$validProps = array(
				"columns",
				"title"
			);
		$this->addValidConfigProperties($validProps);		
	}

	public function setColumns($value) {
    	$this->setExtConfigProperty("columns", $value);
    	return $this;
    }	
   
    public function getColumns() {
    	return $this->getExtConfigProperty("columns");
    }
	
	public function setTitle($value) {
    	$this->setExtConfigProperty("title", $value);
    	return $this;
    }
	
    public function getTitle() {
    	return $this->getExtConfigProperty("title");
    }
	
}//end class

?>