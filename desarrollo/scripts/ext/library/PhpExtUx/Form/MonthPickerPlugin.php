<?php

include_once 'PhpExt/Form/TriggerField.php';

class PhpExtUx_Form_MonthPickerPlugin extends PhpExt_Form_TriggerField 
{

	public function __construct( ) {
		parent::__construct();
	
		$this->setExtClassInfo( "Ext.ux.MonthPickerPlugin", "monthPickerPlugin" );		
		
	}

	
		public function getJavascript() {
	    
			
					
			$className = "Ext.ux.MonthPickerPlugin";		
			
					
			
				$js = "new $className()";
				
					
				return $js;
		    
	    
	}
	
    

	
}//end class