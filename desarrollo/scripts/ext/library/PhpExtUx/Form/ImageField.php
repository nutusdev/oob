<?php

include_once 'PhpExt/Form/Field.php';

class PhpExt_Form_ImageField extends PhpExt_Form_Field 
{	
	
	
	public function __construct() 
	{
		parent::__construct();
		$this->setExtClassInfo("Ext.form.ImageField","imagefield");
		
		$validProps = array('value');
		$this->addValidConfigProperties($validProps);
	}	 	
   	
	public function setValue($value)
	{
		$this->setExtConfigProperty("value", $value);
    	return $this;
	}
	
}//end if

