<?php

include_once 'PhpExt/Button.php';

class PhpExt_Form_FileUpload extends PhpExt_Button 
{

	public function setButtonText($value) {
    	$this->setExtConfigProperty( "buttonText", $value );
    	return $this;
    }	

	public function __construct()
	{		
			parent::__construct();								
			$this->setExtClassInfo("Ext.form.FileUploadField","fileuploadfield");	
			$validProps = array(		    
				"buttonText"
			);
			$this->addValidConfigProperties($validProps);		
	}

}//end class

?>