<?php

include_once 'PhpExt/Form/ComboBox.php';

class PhpExtUx_SuperBoxSelect extends PhpExt_Form_ComboBox{

 public function setValue($value) {
    	$this->setExtConfigProperty("value", $value);
    	return $this;
    }	

public function setHeight($value) {
    	$this->setExtConfigProperty("height", $value);
    	return $this;
    }

public function setWidth($value) {
    	$this->setExtConfigProperty("width", $value);
    	return $this;
    }
	
public function set($variable, $value)
{
		$this->setExtConfigProperty($variable, $value);
		return $this;
}

public function __construct(){		
		parent::__construct();								
		$this->setExtClassInfo("Ext.ux.SuperBoxSelect","superboxselect");		


$validProps = array(		    
			"styleField",
			"classField",
			"extraItemStyle",
			"stackItems",
			"fieldLabel",
			"queryDelay",
			"triggerAction",
			"stackItems",
			"emptyText",
			"name",
			"anchor",
			"forceSelection",
			"allowQueryAll",
			"listeners",
			"extraItemCls",
			"allowBlank",
			"msgTarget",
			"xtype",			
			"resizable",
			"hiddenName",			
			"height",
			"width",
			"store",
			"mode",
			"displayField",
			"displayFieldTpl",
			"valueField",
			"navigateItemsWithTab",
			"value"
		);
		$this->addValidConfigProperties($validProps);		
}

}

?>