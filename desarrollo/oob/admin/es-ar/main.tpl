<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>

<head>
<title>{$title}</title>
<meta http-equiv="Content-Type" content="text/html; charset={$encoding}" />
<META name="Copyright" content="Nutus">
</head>

<body>

{literal}
<style>
#loading-mask{
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height:100%;
    z-index:20000;
    background-color:white;
    }
#loading{
    position:absolute;
    left:45%;
    top:40%;
    padding:2px;
    z-index:20001;
    height:auto;
}
#loading a {
	color:#225588;
}
	
#loading .loading-indicator{
    background:white;
    color:#444;
    font:bold 13px tahoma,arial,helvetica;
    padding:10px;
    margin:0;
   height:auto;
}

#loading-msg {
	font: normal 10px arial,tahoma,sans-serif;
}

</style>
<script type="text/javascript">
 var needToConfirm = true;

  window.onbeforeunload = confirmExit;
  function confirmExit()
  {
    if (needToConfirm)
    {
		return "Está seguro que desea salir de esta ventana? Todos sus cambios se perderán";
	}
  }
</script>

{/literal}
<div id="loading-mask" style=""></div>
<div id="loading" style="width:250px;">
    <div class="loading-indicator"><img src="/images/extanim32.gif" width="32" height="32" style="margin-right:8px;float:left;vertical-align:top;"/>
	<a href="http://www.nutus.com.ar">Nutus</a><br/>
	<span id="loading-msg">Cargando bases del sistema</span></div>
</div>


<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/ext-3.3.1/resources/css/ext-all.css"> 
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/ext-3.3.1/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/ext-3.3.1/ext-all-debug-w-comments.js"></script>
<link href="{$webdir}/oob/admin/styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Cargando núcleo...';</script>


<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/toolbar/StatusBar.js"></script>
<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/toolbar/StatusBar.css"> 

<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/menu/EditableItem.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/menu/RangeMenu.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/menu/ListMenu.js"></script>

<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/menu/Ext.ux.MenuPanel/Ext.ux.MenuPanel.js"></script>

<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Cargando componentes...';</script>

<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/GridFilters.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/Filter.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/StringFilter.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/DateFilter.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/ListFilter.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/NumericFilter.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/BooleanFilter.js"></script>


<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/gallery/DataView-more.js"></script>
<link href="{$webdir}/scripts/ext/resources/extjs-ux/gallery/data-view.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/ajax/ext-basex.js"></script>

<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/nutus/Ext.form.Action.Submit.js"></script>

<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/grid/filter/resources/style.css"> 

<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/form/fileuploadfield.css"> 

<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/Multiselect/Multiselect.css"> 
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/Multiselect/DDView.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/Multiselect/ItemSelector.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/Multiselect/Multiselect.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/Multiselect/Multiselectfield.js"></script>

<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/app/ext-searchfield.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/app/fit-to-parent.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/grid/CheckColumn.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/MaskFormattedTextField.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/InputTextMask.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/Ext.ux.UploadPanel.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/Ext.ux.FileUploader.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/Ext.ux.form.BrowseButton.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/Ext.ux.ImageField.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/MonthPlugin.js"></script>


<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/FileUploadField.js"></script>

<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/form/logindialog/Ext.ux.form.LoginDialog.css"> 
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/logindialog/Ext.ux.form.LoginDialog.js"></script>
<script type="text/javascript" src="{$webdir}/scripts/ext/resources/extjs-ux/form/logindialog/Ext.ux.form.IconCombo.js"></script>

<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/form/css/filetree.css"> 
<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/ext/resources/extjs-ux/form/css/icons.css"> 

<script type="text/javascript" src="{$webdir}/scripts/SuperBoxSelect/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$webdir}/scripts/SuperBoxSelect/superboxselect.css"> 

<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Iniciando...';</script>


<!--CARGO EL SCRIPT GENERADO POR PHP-EXT-->
<script type="text/javascript" src="{$webdir}/admin/script"></script>

</body>
</html>