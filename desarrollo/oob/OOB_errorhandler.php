<?
/**
########################################
#OOB/N1 Framework [©2004,2012]
#
#  @copyright Pablo Micolini
#  @license BSD
#  @version 1.2
######################################## 
*/

/**
This class can handle user-errors that wont be throwing an exception. 
That is, that have been taken in consideration by the programer.
*/
class OOB_errorhandler {
	
	private $error = array();
	private $logfile;
	
	public function __construct ( $path = '') 	
	{
		
		$this->logfile = $path . DIRECTORY_SEPARATOR . "error.log";
	}
	/** Adds an error to the handler, you must provide, the "parent (object instance that is having the error)", "message" and (true-false) if you want to log the error. */
	public function addError ($var, $msg, $log = false) 
	{
		$temp['var'] = $var;
		$temp['msg'] = $msg;
		$this->error[] = $temp;
		
		if ($log)
		{
			
			$today = date("Y-m-d H:i:s");
			
			$error_message = array();
			$error_message[] = $today;
			$error_message[] = 'Error';
			$error_message[] = $_SERVER['REMOTE_ADDR'];
			
			$error_message[] = $var;
			$error_message[] = $msg;
			
			error_log(join(',',$error_message) . "\r\n", 3, $this->logfile );
		}
	}
	
	/** Returs all the errors that the handler has */
	public function getErrors () 
	{
		return $this->error; 
	}
	
	/** Returns true if there are any errors in the handler */ 
	public function areError () 
	{
		if (count($this->error) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/** Returns all the errors that the handler has for a given "parent" (object instance) */
	public function getErrorsfor ($var) 
	{
		$return = array();
		foreach ($this->error as $erro)
		{
			if ($erro["var"] == $var)
			{
				$return[] = $erro["msg"];
			}
		}
		
		if (count ($return)>0)
		{
			return $return;
		}
		else 
		{
			return false;
		}
	}
	
	/** Stores a log file, for a given generator, on the logs directory, separated by year/month/day/app */
	static public function logAction($generator,$data,$file_name_extra = false, $extension = 'log')
	{
		global $ari;
		
		$year = date('Y');
		$month = date ('m');
		$day = date ('d');
		
		
		$directory = 'archivos'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $day . DIRECTORY_SEPARATOR . $generator;
		
		if (!is_dir ($directory))
		{
			if (!@mkdir ($directory, '0777', true))
			{
				$ari->error->addError('LOG-ACTION','No se pudo crear el directorio para el registro de ' . $generator,true);
			}
		}
		
		$file_name = $generator . '_' . microtime(true); // .'_'. rand(1000,9999);
		if ($file_name_extra != false)
		{
			$file_name .= '_' . $file_name_extra;
		}
		
		return file_put_contents($directory.DIRECTORY_SEPARATOR.$file_name.'.'.$extension,$data);
	}

}


?>