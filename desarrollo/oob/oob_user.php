<?
#OOB/N1 Framework [©2004,2012 - Nutus]
/**
 @license: BSD
 @author: Pablo Micolini (pablo.micolini@nutus.com.ar)
 @version: 1.4.1

 Provides user validation (login/logout)
*/

class personnel_employee {}

class OOB_user extends OOB_internalerror {
		
	private $user= ID_UNDEFINED;
	private $uname = '';
	private $password = '';
	private $password_md5 = '';
	private $email = '';
	private $session = '';
	private $status= 8; // 0 neeeds auth, 1 ok, 2 temp block, 9 deleted
	private $maxcon= 5;
	private $block_method= 1; // 0 no block, 1 time, 2 statuschange 
	private $block_time= "3600"; //10 minutos
	private $source= "db"; // db, imap
	private $new_validation= "no"; // 0 no, 1 mail, 3 admin
	private $employee = NO_OBJECT;
	
	public function id()
	{
		return $this->get('user');
	}
	
	public function name()
	{
		return $this->get('uname');
	}
	
	/** loads config details, and starts the user. 
 	    if no user set we must believe is a new one */
	public function __construct($user= ID_UNDEFINED) {
		global $ari;
		//load config vars!
		$this->block_method= $ari->config->get('block-method', 'user');
		$this->block_time= $ari->config->get('block-time', 'user');
		$this->source= $ari->config->get('validation-method', 'user');
		$this->new_validation= $ari->config->get('new_validation', 'user');
		if ($user > ID_MINIMAL) {
			$this->user= $user;
			
			if (!$this->fill ())
					{throw new OOB_exception("Invalir user {$user}", "403", "Invalid User", true);}
					
		}  
	}
		
	/** user validation made, username and password must be provided */
	public static function login($uname, $pass) {
		global $ari;
		
	
		
		// clean vars
		if (OOB_validatetext :: isClean($uname) && OOB_validatetext :: isClean($pass) && OOB_validatetext :: isPassword($pass)) {
			
			$imap_pass = $pass;
			$pass= md5($pass); 
		
		} else
			return false;
			
			// load config vars
			$source= $ari->config->get('validation-method', 'user');
			$blocktime= $ari->config->get('block-time', 'user');
			$savem = $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$uname = $ari->db->qMagic ($uname);
			
			// get id where user and pass matches (using proper auth method!)
		
			$sql= "SELECT id, password, status FROM oob_user_user WHERE uname = $uname";
			$ari->db->SetFetchMode($savem);
			$rs = $ari->db->Execute($sql);
			
			if (!$rs || $rs->EOF) {
				return false;
			
			} else {
				$userid= $rs->fields[0];
				$password= $rs->fields[1];
				$status= $rs->fields[2];
				$rs->Close();
				
			}
			switch ($source)
			{
				//lo de siempre es una db	
				case "db":
				default:
				 {
							
					if ($password != $pass) 
						{
						// add failed for uid! if pass not match!
						self::addFailed ($userid, "WRONGPASS");
					
						//contamos cuantos fallados tiene, y 
						// si son mas de los q deberia, lo sentimos mucho
						$fallados = self::getFailed ($userid, $blocktime); 
					
						if ($fallados > 10)
							{throw new OOB_exception("BLOCKED User ($uname)", "403", "Su usuario ha sido desactivado, contacte al administrador", true);}
					
						return false;				
				
				
						}
					break;
					}
					
//				case "imap": {
//					
//					$server = $ari->get("config")->get('imap-server', 'user');
//				
//					//if ok, continue, else add failed as pass not match
//					if ($mbox = @imap_open("{" . $server . "}INBOX", "$uname", "$imap_pass", OP_READONLY))
//					{
//						@imap_close ($mbox);
//						
//						 } else {
//						$now = $ari->db->DBTimeStamp(time());
//						$sql = "insert into oob_user_failed (id, user_id, timestamp , reason )
//								values ('','$userid',$now,'IMAP_INVALID')";
//						if ($ari->db->Execute($sql) === false)
//						throw new OOB_exception("Error en DB: {$ari->db->ErrorMsg()}", "010", "Error en la Base de Datos", false);
//					
//						return false;
//					
//					}
//				
//				}
			}
			


			if ($status != 1) {
			throw new OOB_exception("DISABLED", "403", "Su usuario no está activo, contacte al administrador", false);
			}
			
//valida el permiso de acceso al modulo admin		
			if ($ari->mode == 'admin' ) 
			{
				//verificar permiso
						if ( !seguridad :: isAllowed(seguridad_action :: nameConstructor('adminaccess','access','admin'), '',  new oob_user ($userid)) )
				{
					throw new OOB_exception("Acceso denegado a Usuario {$uname}", "403", "Acceso denegado. Consulte con su administrador", true);
						
				}					
			}		
			
			
			$sessionkey = session_id();
			
			// Con esto grabamos los datos de sesion, para poder loguear al usuario sin problemas 
			session_write_close(); 
			
			
			//stores the login->session key relation (if there is one previously, updates the record.)
			$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$sql= "SELECT user_id FROM oob_user_online WHERE sesskey  = '$sessionkey'";
			$ari->db->SetFetchMode($savem);
			$rs = $ari->db->Execute($sql);

			if (!$rs || $rs->EOF) {
				
					$sql = "INSERT INTO oob_user_online (user_id,sesskey )
							values ('$userid','$sessionkey')";

					if ($ari->db->Execute($sql) === false)
					
					throw new OOB_exception("Error en DB: {$ari->db->ErrorMsg()}", "010", "Error en la Base de Datos, o en la asignación de Sesion, recargue la página", false);

				
								
					return  new oob_user ($userid);
			
			} else {
				$useridlogued= $rs->fields[0];
				$rs->Close();
				
							}
						
					if ($useridlogued == $userid)
						return   new oob_user ($userid);
					else {
						$sql= "UPDATE oob_user_online SET  user_id = '$userid' WHERE sesskey  = '$sessionkey'";

						if ($ari->db->Execute($sql) === false)
							throw new OOB_exception("Error en DB: ". $ari->db->ErrorMsg(), "010", "Error en la Base de Datos", false);
					
						return  new oob_user ($userid);
					
						}					


	}	
	
	/** deletes the sessionid->login link.
	 * You can provide a redirection url, default is home  */
	public function logout($redirect ='/') {

				global $ari;
			$sessionkey = session_id();
			$sql= "DELETE FROM oob_user_online WHERE sesskey  = '$sessionkey' AND  user_id = '$this->user'";
			$ari->db->Execute($sql);
			
			@session_destroy();
			
			if($redirect !== false)
			{	if ($ari->mode == 'admin')
				{
					header( "Location: " . $ari->adminaddress . $redirect);
					exit;	
				
				}
				else
				{
					header( "Location: " . $ari->webaddress . $redirect);
					exit;	
				}	
			}
	}
	
	public function kickOut() 
	{
		global $ari;
			
		$sql= "DELETE FROM oob_user_online WHERE user_id = '$this->user'";
		$ari->db->Execute($sql);	
	}

	/** 
 	look for session id->login link, 
 	if exist return user-object, else false.  */
	 public static function isLogued() {
		
		global $ari;
		
			$sessionkey = session_id();
			$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$sessionkey = $ari->db->qMagic ($sessionkey);
			$sql= "SELECT user_id FROM oob_user_online WHERE sesskey  = $sessionkey";
			$rs = $ari->db->Execute($sql); 
			
			$ari->db->SetFetchMode($savem);
					
				if (!$rs || $rs->EOF) {
				return false;
			}
			
			if (!$rs->EOF && $rs->RecordCount() == 1) {
			
			$userid = $rs->fields[0];
				$rs->Close();
				return new oob_user ($userid);

			}
		//	return false; // por las dudas?
			throw new OOB_exception("Mas de un Usuario para la misma Sesión", "501", "Ha ocurrido un error inesperado", true);
		
		
	}
	
	public static function lostPass($email) 
	{ 	
		global $ari;
		
		if ( !OOB_validatetext :: isEmail($email)  )
			return false;
		
		// valida que exista el usuario o mail
		$id = self::uniqueUser(null,$email, true);
		
		if ($id !== true)
		{
			// genera un codigo unico y lo almacena
			$return = array();
			$return[0] = new OOB_user ($id);
			$id =  $ari->db->qMagic($id);
			$string = md5(time() . $email . 'string aleatorio del programador');
			$return[1] =$string;
			$string = $ari->db->qMagic($string);
			
			$sql= "INSERT INTO oob_user_forgot 
						   ( userID, code)
						   VALUES ( $id, $string )
						   	";
			$ari->db->Execute($sql);
			return $return;
		} 
		else
		{
			return false;
		}
		
	}
	
	/** Verifica que el codigo exista, valida con el usuario y la fecha, 
	 * y si todo va bien, graba la clave nueva.
	 * Le tiene que pasar:
	 * code = md5 - 32 varchar
	 * email = el email registrado
	 * newpass = la clave nueva
	 */
	 public static function validateLost($code, $email, $newpass)
	{
		global $ari;
		// verifica que exista el codigo almacenado
			
			if ( !OOB_validatetext :: isEmail($email)  )
			return false;
			
			if ( !OOB_validatetext :: isClean($code) || !OOB_validatetext :: isCorrectLength($code,32,32))
			return false;
			
			$ari->db->StartTrans();
			$code = $ari->db->qMagic($code);
			
			$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$sql= "SELECT userID,date FROM oob_user_forgot WHERE code  = $code";
			$rs = $ari->db->SelectLimit($sql,1); 
			
			$ari->db->SetFetchMode($savem);
					
				if (!$rs || $rs->EOF) {
				$ari->db->CompleteTrans();
				return false;
			} 
			else 
			{
			$usuario =  new OOB_user ($rs->fields[0]);
			$date = $rs->fields[1];
			$rs->Close();
			}
			
			/////////////////////
			$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$uid = $ari->db->qMagic($usuario->id());
			$sql= "SELECT code FROM oob_user_forgot WHERE userID = $uid  ORDER BY date DESC";
			$rs = $ari->db->SelectLimit($sql,1); 
			
			$ari->db->SetFetchMode($savem);
					
			if (!$rs || $rs->EOF) {
				$ari->db->CompleteTrans();
				return false;
			} 
			else 
			{
			$lastcode = $rs->fields[0];
			$rs->Close();
			}
	
			
			if ($ari->db->qMagic($lastcode) != $code)
			{
				$ari->db->CompleteTrans(); 
				return false;
			}
			
			/////////////////////////
			
			
			
		// valida el email y el vencimiento
		if ($usuario->get('email') != $email)
		{
			$ari->db->CompleteTrans();
			return false;
		}
			
		$ahora = new Date();
		$sudate = new Date ($date);
		
		//@todo VERIFICAR!!! 
		$ahora->getTime();
		$sudate->getTime(); 
		$elminimo = $ahora->getTime() - 86400;
		
//		var_dump($ahora->getTime());
//		var_dump($sudate->getTime());
//		exit;
		
		if ($sudate->getTime() <= $elminimo)
		{$ari->db->CompleteTrans();
		return false;
		}
		else
		{
			// actualiza la clave
		$usuario->set('password', $newpass);
		if ($usuario->store())
			{
				
			// borra los codigos de la db
				
				$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
				$sql= "DELETE FROM `oob_user_forgot` WHERE `userID` = $uid";
				$ari->db->Execute($sql); 
				
				if ($ari->db->CompleteTrans())
				return true;
				else
				return false;
				
				
			}
			else
			{
				return false;
			}
		}

	} // end function
	
	 /** valida que no haya otro usuario con el mismo user o email 
	 *  devuelve TRUE si NO existe el user.
	 * De existir devuelve el ID */
	public static function uniqueUser($user,$email, $both = false)
	{
		global $ari;
		
		$clausula = "OR";
		if ($both == true)
		{	$clausula = "AND";
		}
		
		if ($user == null || $email == null)
		{	$clausula = "";
		}
		
		if (!OOB_validatetext :: isEmail($email)  )
		{	return false;
		}
			
		if (!OOB_validatetext :: isClean($user))
		{	return false;	
		}
			
		if ($user != null)
		{	$uname = $ari->db->qMagic($user);
			$clausulaUser = "uname = $uname";
		}
		else
		{	$clausulaUser = "";
		}
		
		if ($email != null)
		{	$email = $ari->db->qMagic($email);
			$clausulaEmail = "email = $email";
		} 
		else
		{	$clausulaEmail = "";
		}		 
		
		$sql= "SELECT id FROM oob_user_user WHERE  $clausulaUser $clausula $clausulaEmail" ;
			
		$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
		$rs = $ari->db->Execute($sql);
		$ari->db->SetFetchMode($savem);
		
//		var_dump ($rs->EOF);
//		var_dump($rs->fields[0]);
		if (!$rs->EOF && $rs->fields[0]!= 0) 
		{
			$rs->Close();
			return $rs->fields[0];
		} 
		else
		{
			return true;
		}
			


	}



	 /** Valida que no haya otro usuario con el mismo nombre de usuario o email 
	  *  Devuelve TRUE si NO existe el user. De existir devuelve false
	  *  Tiene en cuenta si en nuevo usuario o existente
	  */
	public function isUnique()
	{
		global $ari;
		//validar variables
		if (!OOB_validatetext :: isClean($this->uname) || 
			!OOB_validatetext :: isCorrectLength ($this->uname, 4, 64) ||
			!OOB_validatetext :: isEmail($this->email) )
		{
			return false;
		} 
		
		if ($this->user <> ID_UNDEFINED)
		{//usr existente: verificar contra su mismo id	
			$id = $ari->db->qMagic($this->user);
			$clausula = " AND id <> $id ";
		}
		else
		{//usr nuevo: no importa el id
			$clausula = "";
		}
		
		$uname = $ari->db->qMagic($this->uname);	
		$email = $ari->db->qMagic($this->email);
		
		$sql = "SELECT 1 
				FROM oob_user_user 
				WHERE ( uname = $uname OR email = $email )
				$clausula
			   "; 
		//echo $sql;exit;	   
		$rs = $ari->db->Execute($sql);
		if ($rs && !$rs->EOF) 
		{	$rs->Close();
			return false;
		} 
		else
		{	return true;
		}

	}
	
	public function isTaken()
	{
		global $ari;
		
		if ($this->user <> ID_UNDEFINED)
		{//usr existente: verificar contra su mismo id	
			$id = $ari->db->qMagic($this->user);
			$clausula = " AND id <> $id ";
		}
		else
		{//usr nuevo: no importa el id
			$clausula = "";
		}
		
		$uname = $ari->db->qMagic($this->uname);	
		
		$sql = "SELECT 1 
				FROM oob_user_user 
				WHERE uname = $uname
				$clausula
			   "; 
		   
		$rs = $ari->db->Execute($sql);
		if ($rs && !$rs->EOF) 
		{	$rs->Close();
			return true;
		} 
		else
		{	return false;
		}

	}
	
	public function isMailTaken()
	{
		global $ari;
				
		if ($this->user <> ID_UNDEFINED)
		{//usr existente: verificar contra su mismo id	
			$id = $ari->db->qMagic($this->user);
			$clausula = " AND id <> $id ";
		}
		else
		{//usr nuevo: no importa el id
			$clausula = "";
		}
		
		$email = $ari->db->qMagic($this->email);	
		
		$sql = "SELECT 1 
				FROM oob_user_user 
				WHERE email = $email
				$clausula
			   "; 
		   
		$rs = $ari->db->Execute($sql);
		if ($rs && !$rs->EOF) 
		{	$rs->Close();
			return true;
		} 
		else
		{	return false;
		}

	}
	
	

	
	/** Returs the value for the given var */ 	
 	public function get ($var)
 	{
 		return $this-> $var;
 	}

	/** Sets the variable (var), with the value (value) */ 	
 	public function set ($var, $value)
 	{
		$this->$var= $value;
 	} 	

	/** Stores/Updates user object in the DB */	
	public function store() 
	{
		global $ari;
		// clean vars !
			
		if (!OOB_validatetext :: isEmail($this->email))
		{   
			$this->error()->addError ("INVALID_EMAIL");
		}
		
		if (!OOB_validatetext :: isClean($this->uname) || !OOB_validatetext :: isCorrectLength ($this->uname, 4, 64))
		{	
			$this->error()->addError ("INVALID_USER");
		} 
		
		//validar que el nombre de usuario y password no se repitan para otro usuario
		if (OOB_validatetext :: isClean($this->uname) && 
			OOB_validatetext :: isCorrectLength ($this->uname, 4, 64) &&
			OOB_validatetext :: isEmail($this->email) )
		{
			// OLD VALIDATION WAY
			// if (!$this->isUnique())
			// {	
				// $this->error()->addError ("INVALID_USER");				
			// }
		
			// NEW VALIDATION 
			if ($this->isTaken())
			{
				$this->error()->addError ("USER_TAKEN");				
			}
			
			if ($this->isMailTaken())
			{
				$this->error()->addError ("EMAIL_TAKEN");				
			}
		} 
			 
			
			if ($this->password != -1)
			{
				if (!OOB_validatetext :: isClean($this->password) ||!OOB_validatetext :: isPassword($this->password) )
				{
					$this->error()->addError ( "INVALID_PASS"); 
				}
			}
			   
			if (!OOB_validatetext :: isNumeric($this->status))
			{
				$this->error()->addError ("INVALID_STATUS", true);
			}
			
			//empleado
			$employee_id = $ari->db->qMagic(0);
			
			
			$errorlist= $this->error()->getErrors();
		
			if (count ($errorlist) > 0)
			{ 
				return false; //devuelve un objeto de error con los errores!
			}
			else
			{
			 	
			 	$uname =$ari->db->qMagic($this->uname);
			 	$email =$ari->db->qMagic($this->email);
		 		$status =$ari->db->qMagic($this->status);
			 	
			 	if ($this->password == -1)
			 	{
					$passql = "";
				}
			 	else
			 	{
			 		$password = md5 ($this->password);
					$this->password_md5 = $password;
			 		$passql = "password = '$password',";
			 	}
				
				if ($this->user > 0)
				{
					// update data
					$ari->db->StartTrans();
					$sql= "UPDATE oob_user_user 
						   SET uname = $uname , 
						   $passql 
						   email = $email, 
						   status = $status, 
						   EmployeeID = $employee_id  
						   WHERE id = '$this->user'";
					$ari->db->Execute($sql);
					
					if (!$ari->db->CompleteTrans())
					{
						throw new OOB_exception("Error en DB: {$ari->db->ErrorMsg()}", "010", "Error en la Base de Datos", false); //return false;
					}	
					else
					{
						$this->password = -1;
						if ($this->status != 1)
						{
							$this->kickOut();
						}
						
						
						
						return true;
					}
					
				} else 
				{
					// insert new and set userid with new id
					$password =$ari->db->qMagic($password);
					$ari->db->StartTrans();
										
					$sql= "INSERT INTO oob_user_user 
						   ( uname, password, email, connections, status, EmployeeID)
						   VALUES ( $uname, $password, $email ,'1',$status, $employee_id)
						   	";
					$ari->db->Execute($sql);
					$this->user = $ari->db->Insert_ID();
				
					if (!$ari->db->CompleteTrans())
					{
						throw new OOB_exception("Error en DB: {$ari->db->ErrorMsg()}", "010", "Error en la Base de Datos", false); //return false;
					}
					else
					{
						$this->password = -1;
						
						
						return true;
					}
				}
		
			}

	}//end function


	/** Deletes user object from the DB*/
	public function delete() {
		global $ari;
		// sets status 9 for a user-id
		if ($this->user > 0 && $this->status != 9) {

			$sql= "UPDATE oob_user_user SET  status = '9' WHERE id = '$this->user'";
			if ($ari->db->Execute($sql))
				return true;
			else
				throw new OOB_exception("Error en DB: {$ari->db->ErrorMsg()}", "010", "Error en la Base de Datos", false);
					

		} else {
			if ($this->status == 9)
			$this->error()->addError ("ALREADY_DELETED");
			else
					 $this->error()->addError ( "NO_SUCH_USER");
			
			return false;
		}
	}
	
	/** Returs the users on the system. status = (enabled/deleted/pending/bloqued/all) shows users */
	static public function userList ($status = 'enabled', $sort = 'uname', $pos = 0, $limit = 20)
	{
	global $ari;
	


	if (in_array ($status, oob_user::getStatus ("ALL", false)) && $status != "all")
	{
	$estado = "WHERE status = " . oob_user::getStatus($status, false);
	}
	else
	{
		if ($status == "all")
			{$estado = "";}
		else
			{$estado = "WHERE status = 1";}
	}

	if (in_array ($sort, oob_user::getOrders()))
		$sortby = "ORDER BY $sort";
	else
		$sortby = "ORDER BY uname";

			$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$sql= "SELECT id FROM oob_user_user  $estado $sortby";
			$rs = $ari->db->SelectLimit($sql, $limit, $pos);

			$ari->db->SetFetchMode($savem);
				if ($rs && !$rs->EOF) { // aca cambie sin probar, hay q ver si anda!
					while (!$rs->EOF) {
					$return[] = new oob_user ($rs->fields[0]);
					$rs->MoveNext();
					}			
			$rs->Close();
			} else
			{return false;}

		return $return;
	}

	/** Returs the users on the system. status = (enabled/deleted/pending/bloqued/all) shows users */
	static public function userCount ($status = 'enabled', $sort = 'uname')
	{
	global $ari;
	


	if (in_array ($status, oob_user::getStatus ("ALL", false)) && $status != "all")
	{
	$estado = "WHERE status = " . oob_user::getStatus($status, false);
	}
	else
	{
		if ($status == "all")
			{$estado = "";}
		else
			{$estado = "WHERE status = 1";}
	}

	if (in_array ($sort, oob_user::getOrders()))
		$sortby = "ORDER BY $sort";
	else
		$sortby = "ORDER BY uname";

			$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
			$sql= "SELECT id FROM oob_user_user  $estado $sortby";
			$rs = $ari->db->Execute($sql);

			$ari->db->SetFetchMode($savem);
				if ($rs && !$rs->EOF) { // aca cambie sin probar, hay q ver si anda!
					$return = $rs->RecordCount();	
			$rs->Close();
			} else
			{return false;}

		return $return;
	}

	/** Returs the users on the system. status = (enabled/deleted/pending/bloqued/all) shows users 
		search by $text in id, uname and email fields
	*/
	static public function search ($status = 'enabled', $sort = 'uname', $text = "", $pos = 0, $limit = 20)
	{
		global $ari;
		//status
		if (in_array ($status, oob_user::getStatus ("ALL", false)) && $status != "all")
		{	$estado = "AND status = " . oob_user::getStatus($status, false);
		}
		else
		{	if ($status == "all")
			{	$estado = "";
			}
			else
			{	$estado = "AND status = 1";
			}
		}
		//search text
		$searchText = "";
		if($text <> "")
		{	if(!OOB_validatetext::isClean($text) )
			{	return false;
			}
			else
			{	
				// $text = trim($text);
				// $textID = $ari->db->qMagic($text);
				// $text = $ari->db->qMagic("%" . $text . "%");
				$searchText = "  $text ";
				// $searchText = " AND ( id = $textID OR uname LIKE $text OR email LIKE $text) ";
			}
		}
		//sort by	
		if (in_array ($sort, oob_user::getOrders()))
		{	$sortby = "ORDER BY $sort";
		}
		else
		{	$sortby = "ORDER BY uname";
		}
		
		$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
		$sql= "SELECT id FROM oob_user_user WHERE 1=1 $estado $searchText $sortby ";
		
		
		$rs = $ari->db->SelectLimit($sql, $limit, $pos);

		$ari->db->SetFetchMode($savem);
		if ($rs && !$rs->EOF) 
		{
			while (!$rs->EOF) 
			{	//@optimize => patron factory
				$return[] = new oob_user ($rs->fields[0]);
				$rs->MoveNext();
			}			
			$rs->Close();
		} 
		else
		{	return false;
		}

		return $return;
	}

	/** Count the users on the system. status = (enabled/deleted/pending/bloqued/all) shows users 
		by $text in id, uname and email fields
	*/
	static public function searchCount ($status = 'enabled', $text = "")
	{
		global $ari;
		//status
		if (in_array ($status, oob_user::getStatus ("ALL", false)) && $status != "all")
		{	$estado = "AND status = " . oob_user::getStatus($status, false);
		}
		else
		{
			if ($status == "all")
			{	$estado = "";
			}
			else
			{	$estado = "AND status = 1";
			}
		}
		//search text
		$searchText = "";
		if($text <> "")
		{	if(!OOB_validatetext::isClean($text) )
			{	return false;
			}
			else
			{	
				// $text = trim($text);
				// $textID = $ari->db->qMagic($text);
				// $text = $ari->db->qMagic("%" . $text . "%");
				$searchText = "  $text ";
				// $searchText = " AND ( id = $textID OR uname LIKE $text OR email LIKE $text) ";
			}
		}
		
		$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
		$sql = "SELECT COUNT(*) 
				FROM oob_user_user 
				WHERE 1=1 
				$estado $searchText
				";
		$rs = $ari->db->Execute($sql);
		$ari->db->SetFetchMode($savem);
		if ($rs && !$rs->EOF) 
		{	$return = $rs->fields[0];	
			$rs->Close();
		} 
		else
		{	return 0;
		}

		return $return;
	}

	/** Shows the available sorting ways for users */
	static public function getOrders()
	{
	$return[] = "uname";
	$return[] = "id";
	$return[] = "email";
	$return[] = "status";
	
	return $return;
	}

	/** Shows the available status for users, or all */	
	static public function getMethods()
	{

	$return[] = "enabled";
	$return[] = "deleted";
	$return[] = "pending";
	$return[] = "bloqued";
	$return[] = "all";
	
	return $return;
	}
	
	/** Shows the user status or all available status 
	 * $one = ID or "status_string" returns the user status; or "ALL" to return an array.
	 * $id => if true, returns a number, else a string.
	 * */
		static public function getStatus ( $one = "ALL" ,$id = true) 
	{ // 0 neeeds auth, 1 ok, 2 temp block, 9 deleted

	$return[1] = "enabled";
	$return[2] = "bloqued";
	$return[0] = "pending";
	$return[9] = "deleted";
	
	if ($id != true)
	$return = array_flip ($return);
	
	if ($one !== "ALL")
	
	{
		if ($return[$one] !== "" )
		{$return = $return[$one];}
		else
		{$return =  false;}

	}
	
	
	return $return;
	}

	/** Updates a given array of users (id), with the status provided. */
	static public function updateStatusFor ($data, $status = false)
	{
	global $ari;
	
	if (!in_array($status, oob_user::getMethods()))
	{throw new OOB_exception("User gave a POST value that is unexistant", "403", "Información inválida desde el Usuario", true);}
		$ari->db->StartTrans();
		foreach ($data as $usuario)
		{
		
		$new = new oob_user ($usuario['id']);
			
		if (OOB_validatetext::isNumeric($status))
			{$new->set ('status', $status);}
		
		else
			{$new->set ('status', oob_user::getStatus($status, false));}
		
		if (!$new->store())
			{throw new OOB_exception("System Error - STATUS:{$status} is invalid", "501", "System Error", true);}
		}
		$ari->db->CompleteTrans();

	}
	
	/** Fills the user with the DB data */
	private function fill ()
	{
	global $ari;

			//load info
			$savem= $ari->db->SetFetchMode(ADODB_FETCH_ASSOC);
			$sql= "SELECT Uname, Password, Email, Connections, Status, EmployeeID 
				   FROM OOB_User_User 
				   WHERE id = '$this->user'";
			$rs = $ari->db->Execute($sql);
			
			$ari->db->SetFetchMode($savem);
			if (!$rs || $rs->EOF) 
			{
				return false;
			}
			if (!$rs->EOF) 
			{
				$this->uname= $rs->fields["Uname"];
				$this->password = -1;
				$this->password_md5 = $rs->fields["Password"];
				$this->email= $rs->fields["Email"];
				$this->maxcon= $rs->fields["Connections"];
				$this->status= $rs->fields["Status"];
				$this->employee = false;
			}
			$rs->Close();
			return true;
	
	}

	/** gets the amout of failed login attemps for a given user-id, with a block time */
	private function getFailed ($userid, $blocktime){
	global $ari;
	
					$userid = $ari->db->qMagic ($userid);
					$savem = $ari->db->SetFetchMode(ADODB_FETCH_NUM);
					$block = $ari->db->DBTimeStamp(time() - $blocktime );
					$sql= "SELECT count(id) as cuenta  FROM oob_user_failed WHERE timestamp > $block AND user_id = $userid";
					$ari->db->SetFetchMode($savem);
					$rs = $ari->db->Execute($sql);
					$cuenta = 0;
					if (!$rs->EOF) {
						$cuenta = $rs->fields[0];
						$rs->Close();
					}
	
	return $cuenta;
	}
	
	/** logs a failed attempted login to the Db for a given user */
	private function addFailed ($userid, $message = "UNDEFINED")
	{
		global $ari;
		
						$now = $ari->db->DBTimeStamp(time());
						$msg = $ari->db->qMagic ($message);
						$sql = "INSERT INTO oob_user_failed ( user_id, timestamp , reason )
								values ('$userid',$now,$msg)";
						if ($ari->db->Execute($sql) === false)
							throw new OOB_exception("Error en DB: {$ari->db->ErrorMsg()}", "010", "Error en la Base de Datos", false);
						
	}
	 

  	/**	Devuelve true si ya existe el nombre de usuario pasado como parametro */
	static public function exists ($uName, $clausula = '')
	{
		global $ari;
		
		$uName = $ari->db->qMagic(trim($uName));
		$savem= $ari->db->SetFetchMode(ADODB_FETCH_ASSOC);
		
		$sql= "SELECT True as Existe 
			   FROM OOB_User_User
			   WHERE Uname = $uName $clausula";
		
		$rs = $ari->db->Execute($sql);
		$ari->db->SetFetchMode($savem);

		if (!$rs->EOF && $rs->fields['Existe']) 
		{	$return = true;		}
		else
		{	$return = false;	}
		
		$rs->Close();
		return $return;
	}
	
	/**
     * Esta funcion valida q los empleados q vienen en el string existan
     * el formato de la cadena es la siguiente
     * LastNameEmployee1, FirstNameEmployee1 EMPLOYEE_SEPARATOR LastNameEmployee2, FirstNameEmployee2......
     * La funcion devuelve un array con los usuarios correspondientes a los empleados q se encontraron, 
     * en caso de q un empleado no exista su lugar en el array lo ocupara un valor booleano false
	 */
	static public function getUsersByString( $string )
	{
	  	$users = array();  	 
	  	$string = trim($string);
	  	$i = 0;
	  	 	
	  	if ($string == "")
	  	{	return false;	}
	  	 
	  	//creo un array con los nombres y apellidos de los empleados
	  	$array_employee = explode(EMPLOYEE_SEPARATOR,$string);
	  	
	  	if (! is_array($array_employee) )
	  	{
	  		$array_employee = array();	
	  		$array_employee[0] = $string;	
	  	}//end if

	  	//recorro el array y separo los nombres y los apellidos
	  	foreach($array_employee as $e)
		{	
			$e = trim($e);
			if ($e != "")
			{
				//busco la posicion del separador de nombres
		  	 	$pos_coma = strpos ($e, NAME_SEPARATOR);
		  	 	
		  	 	if ($pos_coma)
				{ //encontro la posicion
		 			$lastName = trim(substr($e,0,$pos_coma));
					$firstName = trim(str_replace( NAME_SEPARATOR, "", substr($e,$pos_coma+1,strlen($e)-1)));
				}
		  	 	else
				{
					//ver q pasa si no tiene coma
		 			$lastName = $e;
		 			$firstName = '';
				}//end if 
				$users[$i]['employeename'] = $lastName ." ". $firstName;
				$users[$i]['object'] = OOB_User :: getUserByEmployeeName($lastName, $firstName);
				$i++;
			}//end if		
		}//end foreach 				
		
		return $users;
	}//end fucntion	
	
	
	
/** Linkea con el grupo standard para usuarios que se generan solo,'
 * y hace falta q pertenezcan a algun lado para darles permisos, vio?
 */
	public function linkStandardGroup ()
	{
		global $ari;
		 

		$id_grupo = $ari->config->get('new-group', 'user');
		$grupo = new seguridad_group ($id_grupo);

		
		if ($grupo->addUser ($this ))
		{		return true;}
		else
		{		return false;}

					
	}

	/** Lista los usuario on-line.
	 * 
	 */
	public function userOnLineList() 
	{
		global $ari;
		$sessionkey = session_id();
		$sql= "SELECT user_id FROM oob_user_online";
		$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);
		$rs = $ari->db->Execute($sql);
		$ari->db->SetFetchMode($savem);
		if ($rs && !$rs->EOF) 
		{ 
			while (!$rs->EOF) 
			{
				$return[] = new oob_user ($rs->fields[0]);
				$rs->MoveNext();
			}			
			$rs->Close();
		}
		else
		{	return false;
		}

		return $return;
		
	}


	/** Retorna la cantidad de usuarios on-line */
	static public function userOnLineCount()
	{
		global $ari;
		$sql= "SELECT COUNT(*) FROM oob_user_online";
		$savem= $ari->db->SetFetchMode(ADODB_FETCH_NUM);  
		$rs = $ari->db->Execute($sql);
		$ari->db->SetFetchMode($savem);
		if ($rs && !$rs->EOF) 
		{	$return = $rs->fields[0];	
			$rs->Close();
		} 
		else
		{	$return = 0;
		}
		return $return;
	
	}//end function
		
	static public function __SQLsearchRemote($field, $comparison, $value, $connector, $type, $remote_class, $remote_attribute = false)
	{
		global $ari;
		$table = 'oob_user_user';
		$sql_data = $sql_join = array();
		
		$operadores = array();
		$operadores["eq"] = "=";
		$operadores["lt"] = "<";
		$operadores["gt"] = ">";
		$operadores["eqgt"] = ">=";
		$operadores["ltgt"] = "<=";
		$operadores["neq"] = "!=";
		
		if ($field == '' || $field =='user') // cosas del diseño, el field es id, pero la variable es user
		{
			$field = 'id';
		}
		
		$remote_table = $remote_class::getTable();
			
		$sql_join[] = 'JOIN ' . $table . ' ON (' . $table. '.id = ' .  $remote_table . '.' . $remote_attribute . ')';
		
		
		
		if ($type == 'list')
		{
			$operador_inicio = ' IN ( ';
			$operador_fin = " ) ";	
		}
		elseif ($type == 'string')
		{
			$value = $ari->db->qMagic('%' .$value. '%');
			$operador_inicio = ' LIKE ';
			$operador_fin = "";	
		}
		else
		{
			$operador_inicio = $operadores[$comparison];
			$operador_fin = "";	
		}	
		
		
		$sql_data[] = ' ' . $connector . ' ' . $table.'.'. $field . $operador_inicio  . $value . $operador_fin;
		
		return array(
					'data' => $sql_data,
					'join' => $sql_join
					);
		
	}
	
	
}//end class
?>