<?php
#OOB/N1 Framework [©2014 - Nutus]
/**
 @license: BSD
 @author: Pablo Micolini (pablo.micolini@nutus.com.ar)
 @version: 3.3

 Provides simil-multi-thread functionality to oob framework.
*/

class OOB_thread
{

	private $func;
	private $arg = "";
	private $location;
	private $fp;
	private $host;
	private $port = 80;
	private $counter = 0;
	private $multi_urls = array();
	private $multi_urls_simple = array();

	public function __construct($host,$url,$port=false)
	{
		// $host = str_ireplace('www','mt',$host); /// debug
		$this->host = $host;
		$this->location = $url;

		if ($port !== false)
		{
			$this->port = $port;
		}
	}
	
	public function addMultiFunction($function,$arguments = false)
	{
		
		$i=0;
		$arg_string = "";
		if ($arguments)
		{
			foreach ($arguments as $argument)
			{
				$arg_string .= "&a[]=". strtr(base64_encode(addslashes(gzcompress(serialize($argument),9))), '+/=', '-_,');
			}
		}
		// global $ari;
		// $ari->error->AddError(session_id() . ' > pre_args',$arg_string, true );
		
		$this->multi_urls[$this->counter]  = $this->host . $this->location."?treadListener=1&f=".urlencode($function).$arg_string;
		$return = $this->counter;
		$this->counter ++;
		return $return;
		
	}
	
	public function getMultiFunctionReturn()
	{
		global $ari;
		set_time_limit(0);
		$start = microtime(true);
		$handles = array();
		$return = false;
		
		$mh = curl_multi_init();
		$urls = $this->multi_urls;
		foreach ($urls as $key => $url)
		{
			$handles[$key] = curl_init($url);

			curl_setopt($handles[$key], CURLOPT_TIMEOUT, 90);
			curl_setopt($handles[$key], CURLOPT_AUTOREFERER, true);
			curl_setopt($handles[$key], CURLOPT_FAILONERROR, true);
			curl_setopt($handles[$key], CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($handles[$key], CURLOPT_RETURNTRANSFER, true);
			curl_setopt($handles[$key], CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($handles[$key], CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($handles[$key], CURLOPT_DNS_CACHE_TIMEOUT, 9999);
			curl_setopt($handles[$key], CURLOPT_MAXCONNECTS, 20);
			curl_setopt($handles[$key],CURLOPT_ENCODING , "gzip");
			
			$httpHeader = array(
					"Content-Type: text/xml; charset=UTF-8",
					"Content-Encoding: UTF-8",
					"User-Agent: OOB_MT_A",
					"Cookie: OOB_Session=OOB_MT_S_{$key}",
					"Connection: Close"
				);
			curl_setopt($handles[$key], CURLOPT_HTTPHEADER, $httpHeader);
		
			curl_multi_add_handle($mh, $handles[$key]);
		}
		
		$running = null;

		do 
		{
			curl_multi_exec($mh, $running);
			sleep(1);
		}
		while ($running > 0);
		
		
		foreach ($handles as $key => $value)
		{
			$handles[$key] = false;
			
			if (is_resource($value))
			{
				if (curl_errno($value) === 0)
				{
					$output = curl_multi_getcontent($value);
					// $handles[$key] = unserialize((stripslashes(base64_decode(strtr($output, '-_,', '+/=')))));
					// $handles[$key] = unserialize(($output));
					$handles[$key] = unserialize(json_decode($output));
				
					if ($handles[$key]['data']['results'] == true && isset($handles[$key]['data']['hash'])  && isset($handles[$key]['data']['dir']))
					{
						$options = array(
						'cacheDir' => $ari->cachedir . DIRECTORY_SEPARATOR . $handles[$key]['data']['dir'] . DIRECTORY_SEPARATOR,
						'lifeTime' => (15*60),
						'fileNameProtection' => false,
						'automaticSerialization' => true
						);
		
						$Cache_Lite = new Cache_Lite($options);
						$handles[$key]['data']['results'] = $Cache_Lite->get($handles[$key]['data']['hash']);
				
					}			
				} // curlerror
				
				curl_multi_remove_handle($mh, $value);
				curl_close($value);
			} // end resource
		}

		curl_multi_close($mh);
		
		$total = (microtime(true) - $start);
		if ($total > 10)
		{
			$ari->error->AddError(session_id() . ' > end threads',$total, true );
		}
		
		return $handles;
	
	}

	static public function treadListener()
	{
		global $ari;
		$time = microtime(true);
		
		$ari->error->AddError(session_id() . ' > empieza listener',(microtime(true) - $time), true );
		
		if (isset($_GET['treadListener']))
		{
			$arg = array();
			if (isset($_GET['a']))
			{
				foreach($_GET['a'] as $argument)
				{
					// verificar que los argumentos se puedan deserializar
					$arg[] =  unserialize(gzuncompress(stripslashes(base64_decode(strtr($argument, '-_,', '+/=')))));
				}

			}
			
			// $ari->error->AddError(session_id() . ' > antes funcion listener',(microtime(true) - $time), true );
			// $ari->error->AddError(session_id() . ' > args',var_export($arg,true), true );
			
			// verificar que la funcion exista
			if (function_exists($_GET["f"]))
			{
				
				$return = call_user_func_array($_GET["f"], $arg);
				
				// $ari->error->AddError(session_id() . ' > despues llamar',(microtime(true) - $time), true );
				
				$results = array ('data'=> $return, 'time' => (microtime(true) - $time));
			
				
				ob_end_clean();
				// $datos =  strtr(base64_encode(addslashes((serialize($results)))), '+/=', '-_,');
				$datos =  json_encode(serialize($results));
				// $datos =  (serialize($results));
				echo $datos;
				
				// $ari->error->AddError(session_id() . ' > despues del echo',(microtime(true) - $time), true );
	
			}
			else
			{
				return false;
			}
			//exit;

		}
		
		
	}

}
?>