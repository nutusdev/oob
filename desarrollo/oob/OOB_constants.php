<?php
/**
########################################
#OOB/N1 Framework [©2004,2006,2012]
#
#  @copyright Pablo Micolini
#  @license BSD
######################################## 
*/

 define("NO_OBJECT",false);
 define("SQL_CACHE",600); // 10 minutes
 define("DELETED",9);
 define("USED",1);
 define("ID_UNDEFINED",-1);
 define("ID_MINIMAL",0);
 define("MAX_LENGTH",255);
 define("MAX_LENGTH_TEXT",4294967295); 
 define("OPERATOR_DISTINCT","<>");
 define("OPERATOR_EQUAL","=");
 define("OPERATOR_GREATER",">");
 define("OPERATOR_SMALLER","<");

 define("ID_OPERATOR_GREATER","1");
 define("ID_OPERATOR_SMALLER","2");
 define("ID_OPERATOR_EQUAL","3");
 
 define("USER",'u');
 define("GROUP",'g');
 define("MODULE",'m');
 define("ROLE",'r');
 define("TEAM",'t');
 define("ACCOUNT",'a');
 define("EMPLOYEE",'e');
 define("CLIENTE",'al');
 define("SERVICIO",'cu');//GAP Mod
 
 define("NO",0);
 define("YES",1);
 
 define("ALL",'all');
 define("CHECKED",'checked');
 define("UNCHECKED",'unchecked');
 define("ANONIMO",1);
 define("NO_ANONIMO",0);
 define("NO_MENU",0);
 define("IN_MENU",1);
 define("ALL_MENU",-1);
 define("TREE_ROOT",0);
 
 //calendar
 define ("JANUARY",1);
 define ("FEBRUARY",2);
 define ("MARCH",3);
 define ("APRIL",4);
 define ("MAY",5);
 define ("JUNE",6);
 define ("JULY",7);
 define ("AUGUST",8);
 define ("SEPTEMBER",9);
 define ("OCTOBER",10);
 define ("NOVEMBER",11);
 define ("DECEMBER",12);
 
 //constantes de los dias
 //para date domingo es 0 y sabado 6
 define ("SUNDAY",0);
 define ("MONDAY",1);
 define ("TUESDAY",2);
 define ("WEDNESDAY",3);
 define ("THURSDAY",4);
 define ("FRIDAY",5);
 define ("SATURDAY",6);

 //NOTA: en mysql => DAYOFWEEK( 'YYYY-MM-DD' )  -> domingo = 1 hasta sabado = 7 
 //		 Date_Calc::dayOfWeek() 				-> domingo = 0 hasta sabado = 6   

 define ("ONLINE_EMAIL",2);

 define ("ACCOUNT_SEPARATOR",";");
 define ("EMPLOYEE_SEPARATOR",";");
 define ("NAME_SEPARATOR",",");
 define ("ID_SEPARATOR_URL","|");
 define ("ITEM_SEPARATOR_URL","_");
  
 //
 define ("FIELD_SEPARATOR","#");
 define ("ITEM_SEPARATOR","@"); 
 define ("ID_SEPARATOR","-");  
 //
 define("FIXED_CHANGE", "1");
 define("FLOAT_CHANGE", "2");
 
  
 //
 define ("ACTION_NEW",'new');
 define ("ACTION_UPDATE",'update');
 define ("ACTION_DELETE",'delete'); 
 define ("ACTION_MOVE",'move'); 
 define ("ACTION_COPY",'copy'); 
 define ("ACTION_RELEASE",'release'); 
 

 define ("ORDER_ASC", "1"); 
 define ("ORDER_DESC", "0");
 
 define("ENCRYPT_SEED", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
 
 define ("CONTENT_HTML", "html"); 
 define ("CONTENT_TEXT", "text"); 


define("ANONIMOUS",-2);
define("STR_ANONIMOUS", "Anonimous");


define("MAX_LENGTH_LONGTEXT",4294967295);
define("OPERATOR_IN","IN");
define("OPERATOR_NOT_IN","NOT IN");

define("LEVEL_INDENT","--"); 

//para metodo Date::compare(d1, d2) 
//(0) => d1 = d2 || (-1) => d1 < d2 || (1) => d1 > d2
define("D1_ISLESS",-1);
define("D1_ISGREATER",1);
define("DATE_EQUALS",0);


define("CHAR_CSVEXPORT", ";");
define("CHAR_CSVEXPORT_CHANGE", "\;");

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

 define("ENABLED",1);

 // order modes
 define ("ASC","ASC");
 define ("DESC","DESC");

 //paginacion de grilla
 define ("PAGE_SIZE",30);
 
 define( "TEXT_OPCION_DESHABILITADA","Esta opci&oacute;n se encuentra deshabilitada");

?>