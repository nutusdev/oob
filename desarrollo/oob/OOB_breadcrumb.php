<?php
/**
########################################
#OOB/N1 Framework [©2012]
#
#  @copyright Pablo Micolini
#  @license BSD
#  @version 1
######################################## 
*/

class OOB_breadcrumb
{
	private $url;
	private $name;
	
	public function __construct($name,$url = '/')
	{
		$this->url = $url;
		$this->name = $name;
	}
	
	public function url () 
	{
		return $this->url;
	}
	
	public function name () 
	{
		return $this->name;
	}
	 
}

?>