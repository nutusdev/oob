<?php
/**
 * Project: Smarty memcached cache handler function
 * Author: Mads Sülau Jørgensen <php at mads dot sulau dot dk>
 * Updated: 27/3-2007 - rewritten to be more sane.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
function memcache_cache_handler($action, &$smarty_obj, &$cache_content, $tpl_file=null, $cache_id=null, $compile_id=null, $exp_time=null) 
{
	global $ari;
	$return = false;
	
	if ($ari->memcache == false)
	{
		return false;
	}
		
	// unique cache id
	$cache_id = 'sm_' . md5($tpl_file.$cache_id.$compile_id);

	
	switch ($action) {
	case 'read':
		// grab the key from memcached
		$cache_content = @$ari->memcache->get($cache_id);
		$return = true;
		break;
	
	case 'write':
		if(!@$ari->memcache->set($cache_id, $cache_content, 0, (int)$exp_time)) 
		{
			$ari->error->addError('SMARTY-MEMCACHE',"cache_handler: set failed.",true);
		}
		$return = true;
		break;
	
	case 'clear':
		if($cache_id == null) {
			$ari->memcache->flush();
		} else {
			$result = @$ari->memcache->delete($cache_id);
		}
		if(!$result) 
		{
			$ari->error->addError('SMARTY-MEMCACHE',"cache_handler: clear failed.",true);
		}
		$return = true;
		break;
		
	default:
		$smarty_obj->trigger_error("cache_handler: unknown action \"$action\"");
		$return = false;
		break;
	}
	
	return $return;
}
?>