<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty strip_tags modifier plugin
 *
 * Type:     modifier<br>
 * Name:     strip_tags<br>
 * Purpose:  strip html tags from text
 * @link http://smarty.php.net/manual/en/language.modifier.strip.tags.php
 *          strip_tags (Smarty online manual)
 * @param string
 * @param boolean
 * @return string
 */
function smarty_modifier_simplify_text($string)
{
	$string = str_ireplace('<div>','<p>',$string);
	$string = str_ireplace('</div>','</p>',$string);
	$tags = '<br><br/><b><i><a><s><u><ul><li><ol><strong><underline><p><a><img><h1><h2><h3><h4><h5><h6><p>';
    return strip_tags($string,$tags);
}

/* vim: set expandtab: */

?>
