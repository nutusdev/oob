<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage oob_text2image
 * @version 1.1
 */


/**
 * Smarty {oob_text2image} plugin, loads image instead of text when found in dir
 *
 */
function smarty_function_oob_text2image($params, &$smarty)
{
	global $ari;
	
	
    if (empty($params['text'])) 
	{
        //$smarty->_trigger_fatal_error("[plugin:oob_text2image] parameter 'text' cannot be empty");
        return;
    }
    
	$dir = $ari->get('filesdir') . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'text2image'; 
	
	$return = $params['text'];
	
	$text_clean = oob_cleantext::cleanString($params['text']);
	
	$origen = $dir . DIRECTORY_SEPARATOR . $text_clean . '.jpg';
	
	$url = $ari->webdir . '/images/text2image/' . $text_clean . '.jpg';
	
	
	
	if (oob_validatetext::isValidDir($origen))
	{
	// busco si existe el archivo
		
		if (file_exists ($origen))
		{
			$tamano = getimagesize($origen);
 			$h = $tamano[1];
			$w = $tamano[0];
		
			$return = '<img src="' . $url  . '" alt="" width="' . $w . '" height="' . $h . '" />';
		}
	}
	
	print $return;
	
}

/* vim: set expandtab: */

?>
