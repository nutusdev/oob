<?php

function smarty_modifier_rating_large($string,$text = "")
{
    $numero_decimal = (float) $string - (int) $string;
	$numero_entero = (int) $string;
	$i = 0;
	
	
	if ($text != "" && $numero_entero == 0)
	{
		echo $text;
	}
	else
	{	
		while ($i < $numero_entero)
		{
			echo '<img src="/images/rating/star.png" />';
			$i++;
		}
		
		if ($numero_decimal > 0.4)
		{
			$i++;
			echo '<img src="/images/rating/halfstar.png" />';
		}
		
		while ($i < 5)
		{
			echo '<img src="/images/rating/blankstar.png" />';
			$i++;
		}
	}
}

?>
