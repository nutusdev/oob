<?php
	// CacheLite for Memcache v1.1.1

define('CACHE_LITE_ERROR_RETURN', 1);
define('CACHE_LITE_ERROR_DIE', 8);

class Cache_Lite
{
	private $_file;
	private $_id;
	private $_group;
	
	private $_lifeTime = 3600;
	private $_fileNameProtection = true;
    private $_automaticSerialization = false;
    private $memcache = false;
    
    
    // --- Public methods ---

    /**
    * Constructor
    *
    * $options is an assoc. Available options are :
    * $options = array(
    *     'lifeTime' => cache lifetime in seconds (int),
    *     'fileNameProtection' => enable / disable automatic file name protection (boolean),
    *     'automaticSerialization' => enable / disable automatic serialization (boolean),
    * );
    *
    * @param array $options options
    * @access public
    */
    public function __construct($options = array(NULL))
    {
        
		global $ari;
		
		if ($ari->memcache != false)
		{
			$this->memcache = $ari->memcache;
		}
		else
		{
			throw new OOB_exception("Memcache no está configurado! Va a funcionar todo muy lento...", "020", "Existe un inconveniente técnico, porfavor regrese en unos minutos.", true);
		}
		
		foreach($options as $key => $value) {
            $this->setOption($key, $value);
        }
		
    }
	
	protected function setOption($name, $value) 
    {
        $availableOptions = array('fileNameProtection', 'lifeTime','automaticSerialization');
        if (in_array($name, $availableOptions)) {
            $property = '_'.$name;
            $this->$property = $value;
        }
    }
     
    /**
    * Test if a cache is available and (if yes) return it
    *
    * @param string $id cache id
    * @param string $group name of the cache group
    * @param boolean $doNotTestCacheValidity if set to true, the cache validity won't be tested
    * @return string data of the cache (else : false)
    * @access public
    */
    public function get($id, $group = 'default', $doNotTestCacheValidity = false)
    {
		$this->_id = $id;
        $this->_group = $group;
		$this->_setKeyName($id, $group);
        $data = false;
		
		if ($doNotTestCacheValidity)
		{
			throw new OOB_exception("Memcache no permite doNotTestCacheValidity", "021", "Existe un inconveniente técnico, porfavor regrese en unos minutos.", true);
		}
		
        if ($data = @$this->memcache->get($this->_file)) 
		{
            // if (($this->_automaticSerialization) and (is_string($data))) 
			// {
                // $data = unserialize($data);
            // }		
            return $data;
        }
		
        return false;
    }
    
    /**
    * Save some data in a cache file
    *
    * @param string $data data to put in cache (can be another type than strings if automaticSerialization is on)
    * @param string $id cache id
    * @param string $group name of the cache group
    * @return boolean true if no problem (else : false or a PEAR_Error object)
    * @access public
    */
    public function save($data, $id = NULL, $group = 'default')
    {
		global $ari;
		
		if ($id == NULL)
		{
			// return false;
			throw new OOB_exception("Memcache no permite id=NULL", "021", "Existe un inconveniente técnico, porfavor regrese en unos minutos.", true);
		}
			
		$this->_setKeyName($id, $group);
	
		if (@$this->memcache->set($this->_file,$data,MEMCACHE_COMPRESSED,$this->_lifeTime)) 
		{
			return true;
		}
		else
		{
			$ari->error->AddError('Cache',$this->_file,true);
			file_put_contents('no_graba_'.microtime(true).'.txt',serialize($data));
		}
	
        return false;
    }
	
	public function remove($id, $group = 'default', $checkbeforeunlink = false)
	{
	
		if ($id == NULL)
		{
			// return false;
			throw new OOB_exception("Memcache no permite id=NULL", "021", "Existe un inconveniente técnico, porfavor regrese en unos minutos.", true);
		}
			
		$this->_setKeyName($id, $group);
		
		// if (@$this->memcache->delete($this->_file)) 
		if (@$this->memcache->set($this->_file,'',0,0))
		{
			return true;
		}
		
		return false;
	}

   
    private function _setKeyName($id, $group)
    {
        if ($this->_fileNameProtection) 
		{
            $this->_file = md5($group).'_'.md5($id);
        } 
		else 
		{
            $this->_file = $group.'_'.$id;
        }
    }
    
} 

?>
