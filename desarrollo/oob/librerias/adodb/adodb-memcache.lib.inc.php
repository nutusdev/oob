<?php

// security - hide paths
if (!defined('ADODB_DIR')) die();

global $ADODB_INCLUDED_MEMCACHE;
$ADODB_INCLUDED_MEMCACHE = 1;

global $ADODB_INCLUDED_CSV;
if (empty($ADODB_INCLUDED_CSV)) include(ADODB_DIR.'/adodb-csvlib.inc.php');

/* 

  V5.06 16 Oct 2008  (c) 2000-2010 John Lim (jlim#natsoft.com). All rights reserved.
  Released under both BSD license and Lesser GPL library license. 
  Whenever there is any discrepancy between the two licenses, 
  the BSD license will take precedence. See License.txt. 
  Set tabs to 4 for best viewing.
  
  Latest version is available at http://adodb.sourceforge.net

Usage:
  
$db = NewADOConnection($driver);
$db->memCache = true; /// should we use memCache instead of caching in files
$db->memCacheHost = array($ip1, $ip2, $ip3);
$db->memCachePort = 11211; /// this is default memCache port
$db->memCacheCompress = false; /// Use 'true' to store the item compressed (uses zlib)

$db->Connect(...);
$db->CacheExecute($sql);
  
  Note the memcache class is shared by all connections, is created during the first call to Connect/PConnect.
  
  Class instance is stored in $ADODB_CACHE
*/

	class ADODB_Cache_MemCache 
	{
		var $createdir = false; // create caching directory structure?
		
		// memcache specific variables
		var $compress = false;
		var $_connected = false;
		var $_memcache = false;
		
		function ADODB_Cache_MemCache(&$obj)
		{
		}
		
		// implement as lazy connection. The connection only occurs on CacheExecute call
		function connect(&$err)
		{
			global $ari;
			
			if (!function_exists('memcache_pconnect')) 
			{
				$err = 'Memcache extension not found!';
				ADOConnection::outp($err);
				return false;
			}
			
			if ($ari->memcache != false)
			{
				$this->_connected = true;
				$this->_memcache = $ari->memcache;
			}
			
			return true;
		}
		
		// returns true or false. true if successful save
		function writecache($filename, $contents, $debug, $secs2cache)
		{
			if (!$this->_connected) 
			{
				$err = '';
				if (!$this->connect($err) && $debug) ADOConnection::outp($err);
			}
			if (!$this->_memcache) return false;
			
			if ($secs2cache > 0)
			{
				if (!@$this->_memcache->set('ad_'.$filename, $contents, $this->compress, $secs2cache)) 
				{
					if ($debug) 
					{
						ADOConnection::outp(" Failed to save data at the memcached server!");
					}
				}
			}
			return true;
		}
		
		// returns a recordset
		function readcache($filename, &$err, $secs2cache, $rsClass)
		{
			$false = false;
			if (!$this->_connected) $this->connect($err);
			if (!$this->_memcache) return $false;
			
			$rs = @$this->_memcache->get('ad_'.$filename);
			
			if (!$rs) 
			{
				$err = "Item with such key doesn't exists on the memcached server.";
				return $false;
			}
			
			// hack, should actually use _csv2rs
			$rs = explode("\n", $rs);
            unset($rs[0]);
            $rs = join("\n", $rs);
 			$rs = unserialize($rs);
			if (! is_object($rs)) 
			{
				$err = 'Unable to unserialize $rs';		
				return $false;
			}
			
			return $rs;
		}
		
		function flushall($debug=false)
		{
			if (!$this->_connected) 
			{
				$err = '';
				if (!$this->connect($err) && $debug)
				{
					ADOConnection::outp($err);
				}
			}
			if (!$this->_memcache)
			{
				return false;
			}
			
			$del = $this->_memcache->flush();
			
			if ($debug)
			{
				if (!$del)
				{
					ADOConnection::outp("flushall: failed!");
				}
				else
				{
					ADOConnection::outp("flushall: succeeded!");
				}
			}	
			return $del;
		}
		
		function flushcache($filename, $debug=false)
		{
			if (!$this->_connected) {
  				$err = '';
  				if (!$this->connect($err) && $debug) ADOConnection::outp($err); 
			} 
			if (!$this->_memcache) return false;
  
			$del = $this->_memcache->delete('ad_'.$filename);
			
			if ($debug) 
				if (!$del) ADOConnection::outp("flushcache: $key entry doesn't exist on memcached server!");
				else ADOConnection::outp("flushcache: $key entry flushed from memcached server!");
				
			return $del;
		}
		
		// not used for memcache
		function createdir($dir, $hash) 
		{
			return true;
		}
	}

?>