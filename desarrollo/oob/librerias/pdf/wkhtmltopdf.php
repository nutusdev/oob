<?php
/**
########################################
#	Wrapper para generación de PDF - OOB/N1 Framework [©2013]
#
#  @copyright Pablo Micolini
#  @license BSD
#  @version 1.0.1
######################################## 
*/
class wkhtmltopdf
{

	protected $file = false;
	protected $_directory;
	protected $encoding;
	
	public function __construct($origin_stream, $encoding = 'UTF-8')
	{
		global $ari;
		$this->_directory = $ari->get('filesdir') .DIRECTORY_SEPARATOR . 'archivos' . DIRECTORY_SEPARATOR . 'wkhtmltopdf';
		$this->encoding = $encoding;
		
		$file_name = rand(100000,999999) . '.html';
		$this->file = $this->_directory . DIRECTORY_SEPARATOR . $file_name;
		
		file_put_contents($this->file, $origin_stream);
		
	}
	
	public function generate_pdf($return_stream = true, $crop_h = false, $crop_w = false)
	{
		return $this->__generate($return_stream,$crop_h,$crop_w,true);
	}
	
	public function generate_image($return_stream = true, $crop_h = false, $crop_w = false)
	{
		return $this->__generate($return_stream,$crop_h,$crop_w,false);
	}
	
	private function __generate($return_stream = true, $crop_h = false, $crop_w = false, $pdf = true)
	{
		global $ari;
		$return = false;
		
		// @fixme faltan validar y pasar estas variables al sistema.
		// $crop_h = false, $crop_w = false,
		
		$to_run = $ari->get('libsdir') . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . 'wkhtmltopdf.exe';
		if (!$pdf)
		{
			$to_run = $ari->get('libsdir') . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . 'wkhtmltoimage.exe';
		}
		
		$run_script_string = $to_run . ' ' . $this->file . ' ' . $this->file . '.pdf';
		
		// echo $run_script_string; exit;
		// $output = shell_exec($to_run . ' "' . $this->file . '" "' . $this->file . '.pdf"');
		
		exec($run_script_string,$output,$return_code);
		
		// var_dump ($return_code);
		// var_dump ($output); exit;
		
		$return = file_get_contents($this->file . '.pdf');
		

		@unlink($this->file . '.pdf');
		@unlink($this->file);
		
		
		if (!$return_stream)
		{
			ob_end_clean();
			header('Content-Type: application/pdf');
			header('Content-Disposition: inline; ');
			echo $return;
		}
		else
		{
			return $return;
		}
		
	}
}

?>