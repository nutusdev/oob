<?php
/**
########################################
#OOB/N1 Framework [©2004,2006,2014]
#
#  @copyright Pablo Micolini
#  @license BSD
#  @version 1.2
#  Some methods copyright Brandon Wamboldt <brandon.wamboldt@gmail.com>  / Util.php
######################################## 
*/

/**
 This class provides "cleaning" functionality for text/html fields.
Example:
| <?
| 	$ct = new OOB_cleantext();
| 	$text = '<a href="http://www.yahoo.com">http://www.yahoo.com/</a>'
| 			.'<br><b>Yahoo Server</b><br><i>Search Engine</i>';
| 	$allowed_tags = array('<br>','<a>');
| 	echo $ct->dropHTML($text,$allowed_tags);
| 	echo $ct->shortText($text,10);															
| ?>
@license BSD
 */
class OOB_cleantext {
		
	private $dropHTML;
	private $activateLinks;
	public $tags = "<a><br><b><h1><h2><h3><h4><h5><h6><i><img><li><ol><p><strong><table><tr><td><th><u><ul><span><div><em><font><hr><pre><style><sub><sup>";
	public $simpletags = "<b><br><p><span><strong><strike><italic><i><style><u><em>";
	
	public function __construct ($tags = null)
	{
		if ($tags != null)
		$this->tags= $tags;
	}
	
	/**
	* this function will activate all functions 
	* in this class except of shortText function
	*/
	public function cleanAll($str){
		$str = $this->dropHTML($str,$this->tags);
		$str = $this->activateLinks($str);
		return $str;
	}

	/**
	* Takes a string, and does the reverse of the PHP 
	* standard function htmlspecialchars().
	*/
	public function undo_htmlspecialchars($string) {
		$string = preg_replace("/&gt;/i", ">", $string);
		$string = preg_replace("/&lt;/i", "<", $string);
		$string = preg_replace("/&quot;/i", "\"", $string);
		$string = preg_replace("/&amp;/i", "&", $string);
		return $string;
	}

	/**
	* this function will activate all links and 
	* email addresses with <a> tag
	*/
	
	/** Taken from utils.php */
	public function activateLinks( $text )
    {
        $text = preg_replace( '/&apos;/', '&#39;', $text ); // IE does not handle &apos; entity!
        $section_html_pattern = '%# Rev:20100913_0900 github.com/jmrware/LinkifyURL
            # Section text into HTML <A> tags  and everything else.
              (                              # $1: Everything not HTML <A> tag.
                [^<]+(?:(?!<a\b)<[^<]*)*     # non A tag stuff starting with non-"<".
              |      (?:(?!<a\b)<[^<]*)+     # non A tag stuff starting with "<".
              )                              # End $1.
            | (                              # $2: HTML <A...>...</A> tag.
                <a\b[^>]*>                   # <A...> opening tag.
                [^<]*(?:(?!</a\b)<[^<]*)*    # A tag contents.
                </a\s*>                      # </A> closing tag.
              )                              # End $2:
            %ix';

        return preg_replace_callback( $section_html_pattern, array( __CLASS__, '_linkify_callback' ), $text );
    }


    public static function _linkify( $text )
    {
        $url_pattern = '/# Rev:20100913_0900 github.com\/jmrware\/LinkifyURL
            # Match http & ftp URL that is not already linkified.
            # Alternative 1: URL delimited by (parentheses).
            (\() # $1 "(" start delimiter.
            ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+) # $2: URL.
            (\)) # $3: ")" end delimiter.
            | # Alternative 2: URL delimited by [square brackets].
            (\[) # $4: "[" start delimiter.
            ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+) # $5: URL.
            (\]) # $6: "]" end delimiter.
            | # Alternative 3: URL delimited by {curly braces}.
            (\{) # $7: "{" start delimiter.
            ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+) # $8: URL.
            (\}) # $9: "}" end delimiter.
            | # Alternative 4: URL delimited by <angle brackets>.
            (<|&(?:lt|\#60|\#x3c);) # $10: "<" start delimiter (or HTML entity).
            ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+) # $11: URL.
            (>|&(?:gt|\#62|\#x3e);) # $12: ">" end delimiter (or HTML entity).
            | # Alternative 5: URL not delimited by (), [], {} or <>.
            ( # $13: Prefix proving URL not already linked.
            (?: ^ # Can be a beginning of line or string, or
            | [^=\s\'"\]] # a non-"=", non-quote, non-"]", followed by
            ) \s*[\'"]? # optional whitespace and optional quote;
            | [^=\s]\s+ # or... a non-equals sign followed by whitespace.
            ) # End $13. Non-prelinkified-proof prefix.
            ( \b # $14: Other non-delimited URL.
            (?:ht|f)tps?:\/\/ # Required literal http, https, ftp or ftps prefix.
            [a-z0-9\-._~!$\'()*+,;=:\/?#[\]@%]+ # All URI chars except "&" (normal*).
            (?: # Either on a "&" or at the end of URI.
            (?! # Allow a "&" char only if not start of an...
            &(?:gt|\#0*62|\#x0*3e); # HTML ">" entity, or
            | &(?:amp|apos|quot|\#0*3[49]|\#x0*2[27]); # a [&\'"] entity if
            [.!&\',:?;]? # followed by optional punctuation then
            (?:[^a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]|$) # a non-URI char or EOS.
            ) & # If neg-assertion true, match "&" (special).
            [a-z0-9\-._~!$\'()*+,;=:\/?#[\]@%]* # More non-& URI chars (normal*).
            )* # Unroll-the-loop (special normal*)*.
            [a-z0-9\-_~$()*+=\/#[\]@%] # Last char can\'t be [.!&\',;:?]
            ) # End $14. Other non-delimited URL.
            /imx';

        $url_replace = '$1$4$7$10$13<a href="$2$5$8$11$14">$2$5$8$11$14</a>$3$6$9$12';
        return preg_replace( $url_pattern, $url_replace, $text );
    }


    static public function _linkify_callback( $matches )
    {
        if ( isset( $matches[2] ) ) {
            return $matches[2];
        }

        return self::_linkify( $matches[1] );
    }
	
	
	/**
	* this function will drop HTML tags except of
	* of tag in $allowed_tags varible
	* can be also array of tags
	*/
	public function dropHTML($str,$allowed_tags = NULL){
		// @todo : falta que saque los eventos de javascript que puedan existir
		if ($allowed_tags == NULL)
			$allowed_tags = $this->tags;
		
		if(is_array($allowed_tags))
		{
			foreach($allowed_tags as $key => $value)
				{$this->tags .= $value;}
				
			return strip_tags($str,$this->tags);
		}
		else
		{
			return strip_tags($str,$this->tags);
		}
	}
	
	/**
	* This function shortens text into $length at most. 
	* If the text is longer, puts an ellipsis at the end.
	*/
	public function shortText( $string, $length, $append = '' )
    {
        $ret= substr( $string, 0, $length );
        $last_space = strrpos( $ret,' ');

        if ( $last_space !== FALSE && $string != $ret ) 
		{
			$ret=substr( $ret, 0, $last_space );
        }

        if ( $ret != $string ) 
		{
            $ret .= $append;
        }

        return $ret;
    }
	
	/** Removes style and class attributes from a string */
	public function removeAttributes($str)
	{
		   $stripAttrib = "' (style|class)=\"(.*?)\"'i";
		   $str = stripslashes($str);
		   $str = preg_replace($stripAttrib, '', $str);
		   return $str;
	}
	
	/** removes all but alphanumeric */
	public function cleanString($str) 
	{
		return preg_replace('/[^a-zA-Z 0-9]+/', '', $str);
	}

	/** escapes string to be sent to javascript, as_html will convert linebreaks to BR */
	public function escapeJavaScriptText($string, $as_html = false) 
	{
		if (!$as_html)
		{
			return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\"))); 
		}
		else
		{
			return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', nl2br((string)$string)), "\0..\37'\\"))); 
		}
	}

}
?>