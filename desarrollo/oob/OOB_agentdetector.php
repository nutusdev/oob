<?php
/**
########################################
#OOB/N1 Framework [©2004,2006,2014]
#
#  @copyright Pablo Micolini, based on original work by Philip Iezzi <pipo@phpee.com>
#  @note	Only language detection left. Some stuff must be added someday to know if it's a bot visitng the site 4 example.
#  @license      http://opensource.org/licenses/lgpl-license.php GNU Lesser General Public License
######################################## 
*/

class OOB_agentdetector {

	
	private $agent = '';
	private $name = '';
	private $isBot = false;
	private $mainlanguage;
	private $acceptedlang;	
	private $language;	
	private $agt;
	
	public function __construct($agt = '', $main = 'es-ar' , $accepted = '')
	{
		$this->agent  = '';
		$this->name   = '';
		$this->isBot  = false;
		$this->mainlanguage = $main;
		$this->acceptedlang = $accepted;
		$this->agt = $agt;
		
		$this->detectLang();
	}

	/**
	* is agent a bot?
	*
	* @return   boolean
	*/
	public function isBot()
	{
		return $this->isBot; // estaria bueno tener esta funcion...
	}

	
	public function getLang()
	{
		return($this->getSystemLanguage());
	}
	
	public function setLang($value)
	{
		if (in_array ($value, $this->getLanguages()))
		{
			$this->language = $value;
		}
	}
	
	
	public function getSelectedLang ()
	{
		$return =  $this->language;
		if (isset($_SESSION['idioma_instancia']))
		{
			$return = $_SESSION['idioma_instancia'];
		}
		return $return;
	}
	

	public function setSelectedLanguage ($value)
	{
		if (in_array ($value, $this->getLanguages()))
		{
			$_SESSION['idioma_instancia'] = $value;
			$this->setSystemLanguage($value);
		}
	}
	
	
	public function setSystemLanguage ($value)
	{
		if (in_array ($value, $this->getLanguages()))
		{
			$_SESSION['idioma_system'] = $value;
			$_SESSION['idioma_instancia'] = $value;
		}
	}
	
	public function getSystemLanguage ()
	{
		$return =  $this->language;
		if (isset($_SESSION['idioma_system']))
		{
			$return = $_SESSION['idioma_system'];
		}
		return $return;
	}
	
	
	
	private function detectLang()
	{
	
		$fallback = $this->mainlanguage;
		$this->language = $fallback;
		
		$lang_array = explode (',' , $this->mainlanguage . "," . $this->acceptedlang);
		$http_accept_language_array = explode (',', strtolower(@$_SERVER['HTTP_ACCEPT_LANGUAGE']));
		
		if (count ($lang_array) > 0 && count($http_accept_language_array) > 0)
		{
			foreach ($lang_array as $lang_capable)
			{
				if (in_array($lang_capable,$http_accept_language_array))
				{
					$this->language = $lang_capable;
					return true;
				}
			}
			
			// if no real deal with the whole option, we try just the first subset 0,2
			foreach ($lang_array as $lang_capable)
			{
				$lang_capable_start = substr($lang_capable,0,2);
				foreach ($http_accept_language_array as $http_accept_language_option)
				{
					if (substr($http_accept_language_option,0,2) == $lang_capable_start)
					{
						$this->language = $lang_capable;
						return true;
					}
				}
			}
		}
		
	}
	
	/** Devuelve todos los languages aceptados */ 
	public function getLanguages()
	{
		if ($this->acceptedlang != '' || $this->mainlanguage != '')
		{
			// $array_lang = explode(',', $this->acceptedlang);
			// $array_lang[] = $this->mainlanguage;
			// $array_lang = array_unique($array_lang);
			// return  $array_lang;
			return array_unique(explode (',' , $this->mainlanguage . "," . $this->acceptedlang));
		}
		else
		{
			return false;
		}
	}	
	
	public function mainLanguage()
	{
		return $this->mainlanguage;	
	}
	
}
?>