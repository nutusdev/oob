<?
/**
########################################
#OOB/N1 Framework [©2012]
#
#  @copyright Pablo Micolini
#  @license LGPL / BSD
#  @version 1
######################################## 
*/

require_once ('librerias'.DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'class.phpmailer.php');

/** OOB_Mail is a wrapper around PHPMailer to avoid having to set all the config options every time */

class OOB_mail extends PHPMailer 
{
	public function __construct($exceptions = false)
	{
		global $ari;
		parent:: __construct($exceptions);
		
		$this->IsSMTP();
		$this->CharSet    = "UTF-8";
		$this->AltBody   = 'Para ver este mensaje, por favor utilice un lector de correo con capacidad HTML.';
		$this->Host       = $ari->config->get('delivery', 'main');
		
		// $this->Port       = 25;                    // set the SMTP port for the GMAIL server
		// $this->SMTPDebug  = true;// enables SMTP debug information (for testing)
		
		
		// enable SMTP authentication
		$this->SMTPAuth   = true;                  
		$this->Username   = $ari->config->get('smtpuser', 'main');
		$this->Password   = $ari->config->get('smtppass', 'main');
		
		// A default from
		$this->SetFrom($ari->config->get('email', 'main'), $ari->config->get('name', 'main'));
		
	}
}

/* $mail = new PHPMailer(true); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch

  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
  $mail->AddAddress('whoto@otherdomain.com', 'John Doe');
  $mail->SetFrom('name@yourdomain.com', 'First Last');
  $mail->AddCC($agencia_address,$agencia_name);
  $mail->AddBCC($from_address,$from_name);
  $mail->AddReplyTo('name@yourdomain.com', 'First Last');
  $mail->Subject = 'PHPMailer Test Subject via mail(), advanced';
  $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
  $mail->MsgHTML(file_get_contents('contents.html'));
  $mail->AddAttachment('images/phpmailer.gif');      // attachment
  $mail->Send();

 */


?>