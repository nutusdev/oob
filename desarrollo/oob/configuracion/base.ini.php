[main]
title = ""
main_lang = "es-ar"
accepted_lang = "es-ar"
name = ""

allow-cache = "false"
debug = "1"
homeelement ="/about"
urlhandler =""
defaultlogin = "/about"
email = ""
delivery = ""
smtpuser = ""
smtppass = ""

[database]
uri = "mysqli://user:pass@10.7.3.23/dv"


[location]
filesdir = "false"
webaddress = "http://oob"
adminaddress = "http://admin.oob"
cachedir = "false"

[memcache]
server = "127.0.0.1:11211"

[metadata]
description = ""
keywords = ""
author = "Nutus"
expires = "3600"

[user]
validation-method = "db"
new_validation = "yes"
block-method = "time"
block-time = "3600"
imap-server ="localhost:143"
can-self-register = "true"
new-group = "2"