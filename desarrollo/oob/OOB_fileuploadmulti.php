<?
/**
########################################
#OOB/N1 Framework [©2004,2006]
#
#  @copyright Pablo Micolini, based on original work by Lennart Groetzbach
#  @license LGPL
#  @version 1.1
######################################## 
*/

# @author	    Lennart Groetzbach <lennartg@web.de> + Pablo Micolini
# @version 	    0.5 - 2003/01/13

/** Handles the upload of files to the system */ 
class OOB_fileuploadmulti extends OOB_internalerror {
	

	// maximum file size
	private $_size = 1048576;
	private $formVar;
	private $formKey;

	public function __construct($formVar = "file", $formKey = false) {
		if (!ini_get("safe_mode")) {
		} else {
			$this->error()->addError ("CONFIG"); 
			//     $this->_error("Turn 'safemode' in your php.ini off!");
		}
		$this->formVar = $formVar;
		$this->formKey = $formKey;
	}


	////////////////////////////////////////////////////////////////////////
	/**
* Uploads files

* @param    String      $path               dir to move files to
* @param    Boolean     $overwrite          overwrite existing file?   
* @param    Array       $allowedTypeArray   array of allowed file types, if empty all files are allowed
*
* @return   Array       array with file information
* @todo: get base path from config file.
*/
	public function upload($path, $overwrite=false, $allowedTypeArray=null, $prefix = '',$md5 = false ) {
		global $ari;
		// fix path
		$path = str_replace('\\', '/', $path);
		if (substr($path, -1) != '/') {
			$path .= '/';
		}
		if ($prefix != '')
		$prefix = $prefix . '_';
		
		// does upload path exists?
		if ((file_exists($path)) && (is_writable($path))) {

			$res = array();

			if ($this->formKey !== false)
			{
				$file = array(	
				'error' => $_FILES[$this->formVar]['error'][$this->formKey],
				'size' => $_FILES[$this->formVar]['size'][$this->formKey],
				'name' => $_FILES[$this->formVar]['name'][$this->formKey],
				'type' => $_FILES[$this->formVar]['type'][$this->formKey],
				'tmp_name' => $_FILES[$this->formVar]['tmp_name'][$this->formKey]);
			}
			else
			{
				$file = $_FILES[$this->formVar];  
			}
			
			if ($file['error'] == 4) // @todo TA MAL EL PRIMER INDICE EN EXPLORER
			{ 
				$this->error()->addError ("NO_FILE"); 
				return false;
			}
			
			// file_put_contents("subiendo_archivos_males.txt",var_export($this->formKey ,true));exit;
			
			// does the file exist?
			if (!@$file['error'] && $file['size'] && $file['name'] != '') 
			{
				// is the file type allowed?
				if (($allowedTypeArray == null) || (@in_array($file['type'], $allowedTypeArray))) {
					// is it really an uploaded file?
					if (is_uploaded_file($file['tmp_name'])) {
						// does file exists?
						$exists = file_exists($path . $prefix . $file['name']);
						// overwrite file?
						if ($overwrite || !$exists) {
							// move file to new destination
							
							if ($md5)
							{
								// sacamos la extensión
								$info = pathinfo($file['name']);
								move_uploaded_file($file['tmp_name'], $path . $prefix . md5($file['name']) . '.' . $info['extension']);
								$res = array('name' => $prefix . md5($file['name']) . '.' . $info['extension'], 'full_path' => $path . $prefix . md5($file['name']) . '.' . $info['extension'], 'type' => $file['type'], 'size' => $file['size'], 'overwritten' => $exists);
							}	
							else
							{
								move_uploaded_file($file['tmp_name'], $path . $prefix . $file['name']);
								$res = array('name' => $file['name'], 'full_path' => $path .  $prefix . $file['name'], 'type' => $file['type'], 'size' => $file['size'], 'overwritten' => $exists);
							}
							
							// store name, path, type and size information
							

						} else {
							$this->error()->addError ("FILE_EXISTS"); 
							
						}
					} else {
						$this->error()->addError ("NOT_A_FILE"); 
						
					}
				} else {
					$this->error()->addError ("NOT_ALLOWED"); 
					
				}
			} else {
				if (@$file['error'] && $file['error'] != 4) {
					$this->error()->addError ("UNEXISTANT"); 
					
				}
			}

			if (!$this->error()->getErrors())
			{
				if (count ($res) == 0)     
				{
					$this->error()->addError ("UNEXISTANT_1"); 
					return false;
				} else
				{
					return $res;
				}
			}

			else
			{
				$this->error()->addError ("UNEXISTANT_2"); 
				return false;
			}
		}
		$this->error()->addError ("CONFIG"); 
		return false;
	}


}


?>